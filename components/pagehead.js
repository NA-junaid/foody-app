import React from "react";
import Head from "next/head";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";
import ReactHtmlParser from "react-html-parser";

function PageHead() {
  const languages = useSelector((state) => state.languages);
  const filtered = languages.filter((item) => item.isDefault);
  const store = useSelector((state) => state.store);

  const [sheet, setSheet] = useState("");
  let dynamicSheet = `/assets/css/${
    filtered[0].type == "RTL" ? "arabic" : "theme_red"
  }.css`;
  useEffect(() => {
    setSheet(dynamicSheet);
  }, [dynamicSheet, store]);

  let color = store.color;

  if (color != undefined && !color.includes("#")) {
    color = `#${color}`;
  }

  const styles = `
    <style>  
    .custom-control-input:checked ~ .custom-control-label::before {
        border-color: ${color};
        background-color: ${color};
      }
      .primary {
        background: ${color};
      }
      .btn_area .col-md-8 {
        background: ${color};
      }
      .btn_area .col-md-12 {
        background: ${color};
      }      
      .setup-content .btn-primary {
        background: ${color};
      }
      .search-address {
        background: ${color};
      }
      .checked_icon {
        color: ${color};
      }
      .extra_selection .accordion .card .card-header {
        background: ${color};
      }
      .fixed_btn_primary {
        background: ${color};
      }  
      .sub_header {
        background: ${color};
      }   
      .breadcrumb a {
        color: ${color};
      }  
      .btn_area .col-md-4 {
        background: ${color};
        color: #fff;
      }
      .schedule-header{
        background: ${color};
      }
      </style>  
    `;

  const renderStyles = () => {
    return ReactHtmlParser(styles);
  };

  return (
    <Head>
      <meta charSet="utf-8" />
      <title>Menu</title>
      <meta content="public" httpEquiv="Cache-control" />
      <meta content="width=device-width, user-scalable=no" name="viewport" />
      <meta content="Dine Menu, food menu, food " />
      <meta
        content="Dine Menu, food menu, food online food food order"
        name="keywords"
      />
      <link href="https://dinemenu.com/" rel="canonical" />
      <link href={store.favicon} rel="icon" />
      <link href="/assets/css/bootstrap.css" rel="stylesheet" />
      <link href="/assets/css/style.css" rel="stylesheet" />
      <link href="/assets/css/theme1.css" rel="stylesheet" />
      <link
        rel="stylesheet"
        type="text/css"
        href="https://coreui.io/v1/demo/AngularJS_Demo/vendors/css/font-awesome.min.css"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="https://coreui.io/v1/demo/AngularJS_Demo/vendors/css/simple-line-icons.min.css"
      />
      <link href={sheet} rel="stylesheet" ref="pagestyle" id="pagestyle" />
      <link href="/assets/css/toastr.css" rel="stylesheet" />
      {renderStyles()}
    </Head>
  );
}

export default PageHead;
