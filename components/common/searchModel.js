import React, { useState, useEffect } from "react";
import SearchBox from "./searchBox";
import CategoriesArea from "../partials/home/categoriesArea";
import Products from "../partials/home/products";
import CloseButton from "../partials/home/closeButton";
import useTranslation from "../../services/useTranslation";
import useLanguage from "../../services/useLanguage";
export default function SearchModel({ products }) {
  const [filteredProducts, setFilteredProducts] = useState([]);
  const { search_products } = useTranslation();
  const lang = useLanguage();

  const handleChange = (event) => {
    
    if (event.target.value != '') {
      let prods = [];
      products.map(p => {
         
          if(p[lang].name.toLowerCase().includes(event.target.value)){
            prods.push(p)
          }

         return p;
      });
      setFilteredProducts(prods);
    }else{
      setFilteredProducts([]);
    }
  };

  useEffect(() => {
    
  }, [filteredProducts]);

  return (
    <div
      className="modal fade"
      id="openModal"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header ">
            <div className="col-md-12 mt-3">
              <input
                type="text"
                placeholder={search_products}
                className="form-control"
                onChange={() => handleChange(event)}
              />
            </div>
            <button
              type="button"
              className="btn btn-dark"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <div className="panel modal-body " style={{minHeight:'600px'}}>
            {filteredProducts.map((product) => {
              return (
                <div className="card-body" key={product.product_id} >
                  <Products product={product}/>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
