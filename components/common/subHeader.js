import React from "react";
function SubHeader(props) {
  const renderSubLink = () => {
    if (props.hasChildren) {
      return (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          //xmlns:xlink="http://www.w3.org/1999/xlink"
          aria-hidden="true"
          focusable="false"
          width="1em"
          height="1em"
          preserveAspectRatio="xMidYMid meet"
          viewBox="0 0 16 16"
          className="iconify filtericon"
          data-icon="bi:filter-circle"
          data-toggle="modal"
          data-target="#exampleModalCenter"
          //style="vertical-align: -0.125em; transform: rotate(360deg);"
        >
          <g fill="currentColor">
            <path
              fillRule="evenodd"
              d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
            ></path>
            <path
              fillRule="evenodd"
              d="M7 11.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z"
            ></path>
          </g>
        </svg>
      );
    }
  };

  if (props.type == 1) {
    return (
      <div className="sub_header">
        <h1 className="text-center">{props.title}</h1>
      </div>
    );
  } else {
    return (
      <div className="sub_header">
        <div className="d-flex pl-3 pt-3 pr-3 pb-1  justify-content-between">
          <h1 className="text-center">{props.title}</h1>
          {renderSubLink()}
        </div>
      </div>
    );
  }
}
export default SubHeader;
