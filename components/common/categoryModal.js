import { useSelector } from "react-redux";
import { useState } from "react";
import useLanguage from "../../services/useLanguage";
import useTranslation from "../../services/useTranslation";
import { useRouter } from "next/router";

const CategoryModal = ({ categoryId }) => {
  const categoriesList = useSelector((state) => state.categories);
  const category = categoriesList.find((c) => c.id == categoryId);
  let id =
    category != undefined && category.children.length > 0
      ? category.children[0].id
      : 0;
  const [selected, setSelected] = useState(id);
  const Router = useRouter();

  const lang = useLanguage();
  const { Apply_Filters, categories } = useTranslation();

  const applyFilter = () => {
    $("#exampleModalCenter").modal("hide");
    Router.push({
      pathname: "/moreitems",
      query: {
        category_id: selected,
      },
    });
  };

  const renderCategories = () => {
    if (category != undefined && category != null && category.children.length > 0) {
      return category.children.map((cat) => {
        let id = `customRadio-${cat.id}`;
        return (
          <div className="custom-control custom-radio pt-3 pb-3" key={cat.id}>
            <input
              type="radio"
              id={id}
              name="customRadio"
              className="custom-control-input"
              onClick={() => setSelected(cat.id)}
            />
            <label className="custom-control-label" htmlFor={id}>
              {cat[lang]}
            </label>
          </div>
        );
      });
    }
  };

  return (
    <div
      className="modal fade"
      id="exampleModalCenter"
      tabIndex={-1}
      role="dialog"
      aria-labelledby="exampleModalCenterTitle"
      //style={{ display: "block" }}
      aria-modal="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLongTitle">
              {categories}
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">{renderCategories()}</div>
          <div className="modal-footer">
            <button type="button" className="primary" onClick={applyFilter}>
              {Apply_Filters}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CategoryModal;
