import React from "react";
import Link from "next/link";
import LanguageDropDown from "./languageDropDown";
import Router from "next/router";
import { useSelector } from "react-redux";

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function Header() {
  const hasBack = useSelector((state) => state.back);
  const renderBack = () => {
    
    if (hasBack) {
      return (
        <a onClick={() => window.history.back()}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            //xmlns:xlink="http://www.w3.org/1999/xlink"
            aria-hidden="true"
            focusable="false"
            width="1em"
            height="1em"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 24 24"
            className="iconify"
            data-icon="ic:baseline-arrow-back"
            style={{ fontSize:'24px' }}
          >
            <path
              d="M20 11H7.83l5.59-5.59L12 4l-8 8l8 8l1.41-1.41L7.83 13H20v-2z"
              fill="currentColor"
            ></path>
          </svg>
        </a>
      );
    } else {
      return (
        <span onClick={openNav}>
          <i>
            <img loading="lazy" src="/assets/img/menu_icon.svg" alt="menu" />
          </i>
        </span>
      );
    }
  };

  return (
    <div>
      <header className="header">
        <div className="d-flex justify-content-between">
          {renderBack()}
          <div className="cart_language">
            <span className="cart_bag">
              <Link href="/cart">
                <a title="cart">
                  <img loading="lazy" src="/assets/img/bag.svg" alt="cart" />
                </a>
              </Link>
            </span>
            <LanguageDropDown />
          </div>
        </div>
      </header>
    </div>
  );
}

export default Header;
