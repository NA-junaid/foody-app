import React from "react";
import formGroup from "../../common/formGroup";
import useTranslation from "../../../services/useTranslation";
import { Formik } from "formik";
import urlService from "../../../services/urlService";
import networkService from "../../../services/networkService";
import store from "../../../redux/index";

function WhiteWrapper() {
  const {success, old_password, new_password, confirm_new_password , submit} = useTranslation();
  const reduxStore = store;
  const onSubmitApi = async (values) => {
    const response = await networkService.post(
      urlService.postAccountPassword,
      values
    );
    if (response.IsValid) {
      toastr.success(success);
      setTimeout(() => {
        const win = window.location.replace('myaccount');
      }, 800);

      
    }else{
      if(response.Errors != null){
        toastr.error(response.Errors.error);
      }
    }
    
  };
  return (
    <div className="white_wrapper cart">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <br />
            <Formik
              initialValues={{
                password_old: "",
                password_new: "",
                password_confirm: "",
              }}
              validate={(values) => {
                const errors = {};

                if (!values.password_old) {
                  errors.password_old = "Password is required";
                }
                if (!values.password_new) {
                  errors.password_new = "Password is required";
                }
                if (values.password_new != values.password_confirm) {
                  errors.password_confirm = "Confirm password not matched";
                }
                return errors;
              }}
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  onSubmitApi(values);
                  // alert(JSON.stringify(values, null, 2));
                  setSubmitting(false);
                }, 400);
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
              }) => (
                <form className="contactus" onSubmit={handleSubmit}>
                  <formGroup
                    label={old_password}
                    name="password_old"
                    type="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password_old}
                    hint="***************"
                  />
                  <p className="text-danger">
                    {errors.password_old &&
                      touched.password_old &&
                      errors.password_old}
                  </p>

                  <formGroup
                    label={new_password}
                    name="password_new"
                    type="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password_new}
                    hint="***************"
                  />
                  <p className="text-danger">
                    {errors.password_new &&
                      touched.password_new &&
                      errors.password_new}
                  </p>

                  <formGroup
                    label={confirm_new_password}
                    name="password_confirm"
                    type="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password_confirm}
                    hint="***************"
                  />
                  <p className="text-danger">
                    {errors.password_confirm &&
                      touched.password_confirm &&
                      errors.password_confirm}
                  </p>

                  <button
                    className="primary mt-3 mb-3"
                    type="submit"
                    disabled={isSubmitting}
                  >
                   {submit}
                  </button>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}
export default WhiteWrapper;
