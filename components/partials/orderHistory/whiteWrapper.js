import React from "react";
import ListItem from "./listItem";

function WhiteWrapper({ orders }) {
  return (
    orders != null ? 
    <div className="white_wrapper cart">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <ul className="ul-account-list ul-order-list pt-3">
              {orders.map(order => {
                return (
                  <ListItem key={order.id} order={order} />
                  )
              })}
            </ul>
          </div>
        </div>
      </div>
    </div> : ''

  );
}

export default WhiteWrapper;