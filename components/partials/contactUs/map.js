import React, { Component } from "react";
import { withScriptjs, withGoogleMap, GoogleMap, Marker} from "react-google-maps";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import { useSelector, useDispatch } from "react-redux";

function Maps(){

    const [holder, setHolder] = useCheckoutDataHolder();

    const outlets = useSelector((state) => state.outlets);
    let firstoutlet = {...outlets[0]};
    let firstoutletlatitude = firstoutlet.latitude;
    let firstoutletlongitude = firstoutlet.longitude;
    let latlng = {
      lat: parseFloat(firstoutletlatitude),
      lng: parseFloat(firstoutletlongitude)
    }

    

    const MapWithAMarker = withScriptjs(withGoogleMap(props =>
        <GoogleMap
          defaultZoom={15}
          defaultCenter={{...props.latlng}}
        >
          <Marker 
            position={{...props.latlng}}
          />
        </GoogleMap>
      ));
    return(
      <span>
        <div className="map ml-3 mr-3 mt-3 mb-3" style={{ width: '94%',height: '200px'}}>
          <MapWithAMarker 
              latlng={latlng}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc&v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={<div style={{ height: `200px` }} />}
              mapElement={<div style={{ height: `200px` }} />}
            />
        </div>
        </span>
    );
}
export default Maps;


