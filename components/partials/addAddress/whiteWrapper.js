import formGroup from "../../common/formGroup";
import roundedButton from "../../common/roundedButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from 'formik';
import networkService from "../../../services/networkService";
import urlService from "../../../services/urlService";
import store from "../../../redux/index";
function WhiteWrapper() {
    const {success, name, phone, email, area_name, block, street_number, city, country, add_address } = useTranslation();
    const reduxStore = store;

    const onSubmitApi = async (values) => {

      let address = 
          values.street_number + ', ' + 
          values.block + ', ' + 
          values.area_name + ', ' + 
          values.city + ', ' + 
          values.country;

        let data = {
            address: address,
            name: values.name,
            phone: values.phone,
            email: values.email,
            area_name: values.area_name,
            block: values.block,
            street_number: values.street_number,
            city: values.city,
            country: values.country,
          };

      
      const response = await networkService.post(urlService.postAddressAdd,data);
      if (response != null && response.IsValid) {
        toastr.success(success);
        setTimeout(() => {
          const win = window.location.replace('addressbook');
        }, 800);
      }
    }
    

    return (
        <div className="white_wrapper cart">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <br />
                        <Formik
                            initialValues={{ area_name: '', block: '', street_number: '', city: '', country: '' }}
                            validate={values => {
                                const errors = {};
                                if (!values.area_name) {
                                    errors.area_name = 'Area Name is required';
                                }
                                if (!values.block) {
                                    errors.block = 'Block is required';
                                }
                                if (!values.street_number) {
                                    errors.street_number = 'Street Number is required';
                                }
                                if (!values.city) {
                                    errors.city = 'City is required';
                                }
                                if (!values.country) {
                                    errors.country = 'Country is required';
                                }
                                return errors;
                            }}
                            onSubmit={(values, { setSubmitting }) => {
                                setTimeout(() => {
                                    onSubmitApi(values);
                                    // alert(JSON.stringify(values, null, 2));
                                    setSubmitting(false);
                                }, 400);
                            }}
                        >
                            {({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                /* and other goodies */
                            }) => (
                                    <form onSubmit={handleSubmit}>
                                        <FormGroup label={name} name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} hint={name} />
                                        <p className="text-danger">{errors.name && touched.name && errors.name}</p>

                                        <FormGroup label={phone} name="phone" type="number" onChange={handleChange} onBlur={handleBlur} value={values.phone} hint={phone} />
                                        <p className="text-danger">{errors.phone && touched.phone && errors.phone}</p>

                                        <FormGroup label={email} name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} hint={email} />
                                        <p className="text-danger">{errors.email && touched.email && errors.email}</p>

                                        <FormGroup label={area_name} name="area_name" onChange={handleChange} onBlur={handleBlur} value={values.area_name} hint={area_name} />
                                        <p className="text-danger">{errors.area_name && touched.area_name && errors.area_name}</p>

                                        <formGroup label={block} name="block" onChange={handleChange} onBlur={handleBlur} value={values.block} hint={block} />
                                        <p className="text-danger">{errors.block && touched.block && errors.block}</p>

                                        <formGroup label={street_number} name="street_number" onChange={handleChange} onBlur={handleBlur} value={values.street_number} hint={street_number} />
                                        <p className="text-danger">{errors.street_number && touched.street_number && errors.street_number}</p>

                                        <formGroup label={city} name="city" onChange={handleChange} onBlur={handleBlur} value={values.city} hint={city} />
                                        <p className="text-danger">{errors.city && touched.city && errors.city}</p>

                                        <formGroup label={country} name="country" onChange={handleChange} onBlur={handleBlur} value={values.country} hint={country} />
                                        <p className="text-danger">{errors.country && touched.country && errors.country}</p>

                                        <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>
                                            {add_address}
                                        </button>
                                    </form>
                                )}
                        </Formik>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default WhiteWrapper;