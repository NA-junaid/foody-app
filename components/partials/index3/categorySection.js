import React from "react";
import Products from "../home/products";
import useLanguage from "../../../services/useLanguage";
import SubCategory from "./subCategory";
import { useSelector } from "react-redux";

function CategorySection({ parent }) {
  const lang = useLanguage();
  const categories = useSelector((state) => state.categories);
  const products = useSelector((state) => state.products);

  const renderDeepChildren = (category) => {
    return category.children.map((deepcategory) => {
      const filteredProducts = products.filter(
        (p) => p.category_id == deepcategory.id
      );
      let total = filteredProducts.length;

      let ac = `sac1${subcategory.id}`;
      let acHash = `#sac1${subcategory.id}`;

      if (total > 0) {
        return (
          <div>
            <div
              className="card-header collapsed mb-2"
              data-toggle="collapse"
              href={acHash}
            >
              <a className="card-title">
                {deepcategory[lang]}
                {renderTotal(total)}
              </a>
            </div>
            {renderProducts(deepcategory.id)}
            <div
              id={ac}
              className="card-body collapse "
              data-parent="#accordion3"
            >
              {renderProducts(deepcategory.id)}
            </div>
          </div>
        );
      }
    });
  };

  const renderChildren = (category) => {
    return category.children.map((subcategory) => {
      const filteredProducts = products.filter(
        (p) => p.category_id == subcategory.id
      );
      let total = filteredProducts.length;

      let ac = `sac1${subcategory.id}`;
      let acHash = `#sac1${subcategory.id}`;
      if (total > 0) {
        return (
          <div key={'sub-cat-'+ subcategory.id } >
            <div
              className="card-header collapsed mb-2 "
              data-toggle="collapse"
              href={acHash}
            >
              <a className="card-title">
                {subcategory[lang]}
                {renderTotal(total)}
              </a>
            </div>
            <div
              id={ac}
              className="card-body collapse "
              data-parent="#accordion2"
              style={{ padding: "0px" }}
            >
              <div id="accordion3" className="accordion accordion_subcat">
                {renderProducts(subcategory.id)}
                <div className="card mb-0">
                  {renderDeepChildren(subcategory)}
                </div>
              </div>
            </div>
          </div>
        );
      }
    });
  };

  const renderProducts = (categoryId) => {
    let prods = products.filter((p) => p.category_id === categoryId);

    if (prods.length > 0) {
      return prods.map((product) => {
        return (
          <Products product={product} key={`prod-` + product.variant_id} />
        );
      });
    }
  };

  const renderTotal = (total) => {
    if (total) {
      return <span>({total})</span>;
    }
  };

  const render = (parent) => {
    if (categories != undefined) {
      return categories.map((category) => {
        let ac = `ac${category.id}`;
        let acHash = `#ac${category.id}`;
        const filteredProducts = products.filter(
          (p) => p.category_id == category.id
        );
        let total = filteredProducts.length;

        return (
          <div className="panel" key={category.id}>
            <div
              className="card-header collapsed mb-2"
              data-toggle="collapse"
              href={acHash}
            >
              <a className="card-title">
                {category[lang]}
                {renderTotal(total)}
              </a>
            </div>
            <div
              id={ac}
              className="card-body collapse "
              data-parent={parent}
              style={{ padding: "0px" }}
            >
              <div id="accordion2" className="accordion accordion_subcat ">
                {renderProducts(category.id)}
                <div className="card mb-0">{renderChildren(category)}</div>
              </div>
            </div>
          </div>
        );
      });
    }
  };

  return render(parent);
}

export default CategorySection;
