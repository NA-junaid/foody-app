import React from "react";
import Step from "./step";
import useTranslation from "../../../services/useTranslation";
import { useSelector, useDispatch } from "react-redux";
import { setStep } from "../../../redux/actions/checkoutStepAction";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";

function StepWizard() {
  const { address, payment } = useTranslation();
  const contact = 'Contact';
  const checkoutStep = useSelector((state) => state.checkoutStep);
  const dispatch = useDispatch();
  const checkoutData = useSelector((state) => state.checkoutData);

  const changeStep = (step) => {

    if(step > 1 && checkoutData.name == "" ){
      return false;
    }

    if(step > 2 && checkoutData.area == null ){
      return false;
    }
    
    dispatch(setStep(step));
  };

  return (
    <div className="stepwizard">
      <div className="stepwizard-row setup-panel">
        <div className="stepwizard-step">
          <a
            onClick={() => changeStep(1)}
            className={checkoutStep == 1 ? "btn btn-primary btn-circle" : "btn btn-default btn-circle"}
            type="button"
          >
            1
          </a>
          <p>{contact}</p>
        </div>

        <div className="stepwizard-step">
          <a
            onClick={() => changeStep(2)}
            className={checkoutStep == 2 ? "btn btn-primary btn-circle" : "btn btn-default btn-circle"}
            type="button"
          >
            2
          </a>
          <p>{address}</p>
        </div>

        <div className="stepwizard-step">
          <a
            onClick={() => changeStep(3)}
            className={checkoutStep == 3 ? "btn btn-primary btn-circle" : "btn btn-default btn-circle"}
            type="button"
          >
            3
          </a>
          <p>{payment}</p>
        </div>
       
      </div>
    </div>
  );
}
export default StepWizard;
