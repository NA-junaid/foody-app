import CheckoutInfo from "./checkoutInfo";
import FormGroup from "../../common/formGroup";
import ContinueButton from "./continueButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { setStep } from "../../../redux/actions/checkoutStepAction";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";
import urlService from "../../../services/urlService";
import networkService from "../../../services/networkService";
import { useEffect, useState } from "react";

function StepOne() {
  const checkoutData = useSelector((state) => state.checkoutData);
  const { name, phone, email, required, invalid_email,next } = useTranslation();
  const [username, setUsername] = useState();
  const [useremail, setUseremail] = useState();
  const [userphone, setUserphone] = useState();
  const dispatch = useDispatch();

  useEffect(() => {
    setUsername(checkoutData.name ?? "");
    setUseremail(checkoutData.email ?? "");
    setUserphone(checkoutData.phone ?? "");

    setInitialize(true);
  }, [username, useremail, userphone]);

  const setInitialize = async () => {
    const response = null;

    if (response != null) {
      setUsername(response.name);
      setUseremail(response.email);
      setUserphone(response.mobile);
    }
  };

  const onKeyPress = (event) => {
    const keyCode = event.keyCode || event.which;
    const keyValue = String.fromCharCode(keyCode);
    if (/\+|-/.test(keyValue))
      event.preventDefault();
  }  

  return (
    <div className="row setup-content" id="step-1">
      <div className="col-xs-6 ">
        <div className="col-md-12">
          <CheckoutInfo title="Contact Information" subTitle="" />
          <Formik
            enableReinitialize
            initialValues={{
              name: username,
              email: useremail,
              phone: userphone,
            }}
            validate={(values) => {
              const errors = {};
              if (!values.email) {
                errors.email = required;
              } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
              ) {
                errors.email = invalid_email;
              }
              if (!values.name) {
                errors.name = required;
              }
              if (!values.phone) {
                errors.phone = required;
              }

              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                setSubmitting(false);

                checkoutData.name = values.name;
                checkoutData.email = values.email;
                checkoutData.phone = values.phone;

                dispatch(updateCheckoutData(checkoutData));
                dispatch(setStep(2));
              }, 400);
              // onSubmitApi(values)
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => (
              <form className="contactus" onSubmit={handleSubmit}>
                <FormGroup
                  label={name}
                  name="name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.name}
                  hint={name}
                  autocomplete="off"
                />
                <p className="text-danger">
                  {errors.name && touched.name && errors.name}
                </p>

                <FormGroup
                  label={phone}
                  name="phone"
                  type="number"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phone}
                  hint={phone}
                  onKeyPress ={(event) => onKeyPress(event,handleChange)}
                />
                <p className="text-danger">
                  {errors.phone && touched.phone && errors.phone}
                </p>

                <FormGroup
                  label={email}
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  hint={email}
                  autocomplete="off"
                />
                <p className="text-danger">
                  {errors.email && touched.email && errors.email}
                </p>

                <button
                  className="btn btn-primary nextBtn proceedBtn btn-lg pull-right"
                  type="submit"
                  disabled={isSubmitting}
                >
                  {next}
                </button>
              </form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
export default StepOne;
