import React, { Component, useRef, useState, useEffect } from "react";
import GoogleMapReact, { InfoWindow, Marker } from "google-map-react";

import { useSelector, useDispatch } from "react-redux";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";
const AnyReactComponent = ({ text }) => <div>{text}</div>;

function LocationMap() {
  const mapRef = useRef(null);
  const [location, setLocation] = useState({ lat: -34.397, lng: 150.644 });
  const checkoutData = useSelector((state) => state.checkoutData);
  const { latitude, longitude } = checkoutData;
  const dispatch = useDispatch();

  const handleDragEnd = (position) => {
    checkoutData.latitude = position.center.lat();
    checkoutData.longitude = position.center.lng();
    dispatch(updateCheckoutData(checkoutData))
  };

  useEffect(() => {
    getMarkerPosition();
    setLocation({
      lat: parseFloat(checkoutData.latitude),
      lng: parseFloat(checkoutData.longitude),
    });
  }, [latitude, longitude]);

  const getMarkerPosition = () => {
    setTimeout(() => {
      let height = $("#map-container").height();
      let width = $("#map-container").width();
      let increment = checkoutData.pickup ? 60 : 120;

      $(".location").css({ top: (height / 2) + increment, left: width / 2 });
    }, 1);
  };

  return (
    <div className="map mb-5" style={{ width: "100%", height: "200px" }}>
      <div style={{ height: "230px", width: "100%" }} id="map-container">
        <img src="/assets/img/location.png" className="location" />
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc" }}
          center={location}
          zoom={19}
          onDragEnd={handleDragEnd}
        ></GoogleMapReact>
      </div>
    </div>
  );
}
export default LocationMap;
