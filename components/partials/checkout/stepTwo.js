import CheckoutInfo from "./checkoutInfo";
import SearchAddress from "./searchAddress";
import LocationMap from "./locationMap";
import FormGroup from "../../common/formGroup";
import SubmitButton from "./submitButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from "formik";
import { useDispatch } from "react-redux";
import { setStep } from "../../../redux/actions/checkoutStepAction";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

function StepTwo() {
  const {
    area_name,
    block,
    street_number,
    house_or_apartment_no,
    Avenue,
    Special_Direction,
    pickup_address,
    billing_address,
    Precise_faster,
    required,
    next
  } = useTranslation();
  const checkoutData = useSelector((state) => state.checkoutData);
  const dispatch = useDispatch();

  const [latlong, setlatlong] = useState("{}");
  const [blockname, setBlockname] = useState();
  const [streetnumber, setStreetNumber] = useState();
  const [housenumber, setHouseNumber] = useState();
  const [avanue, setAvanue] = useState();
  const [direction, setDirection] = useState();
  const { latitude, longitude } = checkoutData;

  const getAddress = (lat, long) => {
    $.ajax({
      url:
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
        lat +
        "," +
        long +
        "&sensor=false&key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc",
      // $.ajax({ url:'https://maps.googleapis.com/maps/api/geocode/json?latlng=24.4111895,54.582321&sensor=false&key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc',
      success: function (data) {
        let areaInfo = {
          blockname: "",
          streetnumber: "",
          housenumber: "",
          avanue: "",
        };

        let components = data.results[0].address_components;

        

        components.map((value, index) => {
          if (
            value.types[0] == "neighborhood" &&
            value.types[1] == "political"
          ) {
            areaInfo.blockname = value.long_name;
          } else if (value.types[0] == "route") {
            areaInfo.street_number = value.long_name;
          } else if (value.types[0] == "street_number") {
            areaInfo.street_number = value.long_name;
          } else if (value.types[0] == "premise") {
            areaInfo.housenumber = value.long_name;
          }
        });

        //checkoutData.areaInfo = areaInfo;
        checkoutData.latitude = lat;
        checkoutData.longitude = long;

        dispatch(updateCheckoutData(checkoutData));
      },
    });
  };

  useEffect(() => {
    console.log(checkoutData.areaInfo)
    if (checkoutData.areaInfo != null) {
      setBlockname(checkoutData.areaInfo.blockname);
      setStreetNumber(checkoutData.areaInfo.streetnumber);
      setHouseNumber(checkoutData.areaInfo.housenumber);
      setAvanue(checkoutData.areaInfo.avanue);
      setDirection(checkoutData.direction);
    } else {
      getAddress(checkoutData.latitude, checkoutData.longitude);
    }
  });

  useEffect(() => {
    getAddress(checkoutData.latitude, checkoutData.longitude);
  }, [latitude, longitude]);

  
  return (
    <div className="row setup-content" id="step-2">
      <div className="col-xs-6 ">
        <div className="col-md-12">
          {checkoutData.pickup == true ? (
            <span>
              <CheckoutInfo title={pickup_address} />
              <span>
                <p>{checkoutData.area}</p>
              </span>
            </span>
          ) : (
            <span>
              <CheckoutInfo title={billing_address} subTitle={Precise_faster} />
              <SearchAddress />
            </span>
          )}

          <LocationMap latlong={latlong} />
          <Formik
            enableReinitialize
            initialValues={{
              blockname: blockname,
              streetnumber: streetnumber,
              housenumber: housenumber,
              avanue: avanue,
              direction: direction,
            }}
            validate={(values) => {
              const errors = {};

              if (!values.blockname) {
                errors.blockname = required;
              }
              if (!values.streetnumber) {
                errors.streetnumber = required;
              }
              if (!values.housenumber) {
                errors.housenumber = required;
              }
              return errors;
            }}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              setTimeout(() => {
                let areaInfo = {
                  blockname: values.blockname,
                  streetnumber: values.streetnumber,
                  housenumber: values.housenumber,
                  avanue: values.avanue,
                };

                checkoutData.areaInfo = areaInfo;
                checkoutData.direction = values.direction;

                dispatch(updateCheckoutData(checkoutData));

                setSubmitting(false);
                dispatch(setStep(3));
              }, 400);
              resetForm();
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) =>
              checkoutData.pickup == false ? (
                <form className="contactus" onSubmit={handleSubmit}>
                  <div className="form-group">
                    <label className="control-label">
                      {block}
                      <em>*</em>
                    </label>
                    <input
                      id="block"
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={block}
                      name="blockname"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.blockname}
                      hint={block}
                    />
                    <p className="text-danger">
                      {errors.blockname &&
                        touched.blockname &&
                        errors.blockname}
                    </p>
                  </div>
                  <div className="form-group">
                    <label className="control-label">
                      {street_number} <em>*</em>
                    </label>
                    <input
                      id="street"
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={street_number}
                      name="streetnumber"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.streetnumber}
                      hint={street_number}
                    />
                    <p className="text-danger">
                      {errors.streetnumber &&
                        touched.streetnumber &&
                        errors.streetnumber}
                    </p>
                  </div>

                  <div className="form-group">
                    <label className="control-label">
                      {house_or_apartment_no} <em>*</em>
                    </label>
                    <input
                      id="house_detail"
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={house_or_apartment_no}
                      name="housenumber"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.housenumber}
                      hint={house_or_apartment_no}
                    />
                    <p className="text-danger">
                      {errors.housenumber &&
                        touched.housenumber &&
                        errors.housenumber}
                    </p>
                  </div>
                  <div className="form-group">
                    <label className="control-label">{Avenue}</label>
                    <input
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={Avenue}
                      name="avanue"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.avanue}
                      hint="Avenue"
                    />
                  </div>
                  <div className="form-group">
                    <label className="control-label">{Special_Direction}</label>
                    <textarea
                      maxLength={100}
                      rows={4}
                      className="form-control"
                      placeholder={Special_Direction}
                      defaultValue={" "}
                      name="direction"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.direction}
                      hint={Special_Direction}
                    />
                  </div>

                  <button
                    className="btn btn-primary nextBtn proceedBtn btn-lg pull-right"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    {next}
                  </button>
                </form>
              ) : (
                <button
                  onClick={() => dispatch(setStep(3))}
                  className="btn btn-primary nextBtn proceedBtn btn-lg pull-right"
                  type="submit"
                >
                  {next}
                </button>
              )
            }
          </Formik>
        </div>
      </div>
    </div>
  );
}
export default StepTwo;
