import React, { useState, useEffect } from "react";
import useTranslation from "../../../services/useTranslation";

import { useSelector, useDispatch } from "react-redux";
import { setPickup } from "../../../redux/actions/pickUpAction";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";

import Select from "react-select";
import Geocode from "react-geocode";

function SearchAddress() {
  const areas = useSelector((state) => state.areas);
  const checkoutData = useSelector((state) => state.checkoutData);
  const dispatch = useDispatch();
  const [init, setInit] = useState(false);
  const [deliveryAreas, setDeliveryAreas] = useState([]);

  const onChanged = (val) => {
    setSelected(val);
    dispatch(
      setPickup({
        pickup: false,
        outlet: "",
        area: val,
      })
    );
  };

  const getAddress = (lat, long) => {
    $.ajax({
      url:
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
        lat +
        "," +
        long +
        "&sensor=false&key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc",
      // $.ajax({ url:'https://maps.googleapis.com/maps/api/geocode/json?latlng=24.4111895,54.582321&sensor=false&key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc',
      success: function (data) {
        let areaInfo = {
          blockname: "",
          streetnumber: "",
          housenumber: "",
          avanue: "",
        };

        let components = data.results[0].address_components;

        components.map((value, index) => {
          if (
            value.types[0] == "neighborhood" &&
            value.types[1] == "political"
          ) {
            areaInfo.blockname = value.long_name;
          } else if (value.types[0] == "route") {
            areaInfo.street_number = value.long_name;
          } else if (value.types[0] == "street_number") {
            areaInfo.street_number = value.long_name;
          } else if (value.types[0] == "premise") {
            areaInfo.housenumber = value.long_name;
          }
        });

        checkoutData.areaInfo = areaInfo;
        checkoutData.latitude = lat;
        checkoutData.longitude = long;

        dispatch(updateCheckoutData(checkoutData));
      },
    });
  };

  const getAddressByInput = async (keyword) => {
    $.ajax({
      url:
        "https://maps.googleapis.com/maps/api/geocode/json?address=" +
        keyword +
        "&sensor=false&key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc",
      success: function (data) {
        checkoutData.latitude = data.results[0].geometry.location.lat;
        checkoutData.longitude = data.results[0].geometry.location.lng;

        dispatch(updateCheckoutData(checkoutData));
      },
    });
  };

  const handleLocation = () => {
    4;
    navigator.geolocation.getCurrentPosition(function (position) {
      getAddress(position.coords.latitude, position.coords.longitude);
    });
    if (!navigator.geolocation) {
      alert("Geolocation is not supported by this browser.");
    }
  };

  const handleLocationByInput = (val) => {
    getAddressByInput(val);
  };
  const handlePlacesSuggestions = () => {
    setTimeout(() => {
      //googlearea();
    }, 400);
  };

  const { area_name } = useTranslation();

  const renderInput = () => {
    if (deliveryAreas.length > 0) {
      return (
        <select
          className="custom-seelct"
          style={{
            width: "82%",
            height: "55px",
            borderTopLeftRadius: "15px",
            borderBottomLeftRadius: "15px",
          }}
          onChange={(event) => handleLocationByInput(event.target.value)}
        >
          <option>Select</option>
          {deliveryAreas.map((area) => (
            <option
              value={area.name}
              key={area.name}
              selected={checkoutData.area === area.name ? "selected" : ""}
            >
              {area.name}
            </option>
          ))}
        </select>
      );
    } else {
      return (
        <input
          type="text"
          placeholder={area_name}
          name
          onChange={() => handlePlacesSuggestions()}
          id="delivery-area"
        />
      );
    }
  };

  useEffect(() => {
    if (!init) {
      setDeliveryAreas([...areas]);
      setInit(true);
    }
  }, [init]);

  return (
    <div className="search-address mb-4">
      <span className="location_search">
        <a href="#" onClick={() => handleLocation()}>
          <span
            className="iconify"
            data-inline="false"
            data-icon="bx:bx-current-location"
            style={{ color: "#ffffff", fontSize: 35 }}
          />
        </a>
      </span>
      {renderInput()}
    </div>
  );
}
export default SearchAddress;
