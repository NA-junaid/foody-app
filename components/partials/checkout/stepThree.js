import React, { useState, useEffect } from "react";
import PaymentMethod from "./paymentMethod";
import CheckoutInfo from "./checkoutInfo";
import ShippingInfo from "./shipingInfo";
import SubmitButton from "./submitButton";
import roundedButton from "../../common/roundedButton";
import useTranslation from "../../../services/useTranslation";
import { Formik, Field, Form } from "formik";
import { useDispatch, useSelector } from "react-redux";
import RadioButton from "./radioButton";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";
import Router from "next/router";
import useCurrency from "../../../services/useCurrency";

import useCurrencyConverter from "../../../services/useCurrencyConverter";
function StepThree() {
  const {
    payment_method,
    We_updates,
    Review_Order,
    shipping_method,
    required,
  } = useTranslation();

  const currency = useCurrency();
  const store = useSelector((state) => state.store);
  const paymentMethods = useSelector((state) => state.paymentMethods);
  const shippingMethods = useSelector((state) => state.shippingMethods);
  const checkoutData = useSelector((state) => state.checkoutData);

  let shippingMethod = shippingMethods[0].id;

  if (
    checkoutData.shippingMethod != null &&
    checkoutData.shippingMethod != undefined
  ) {
    shippingMethod = checkoutData.shippingMethod;
  }else{
    shippingMethod = shippingMethods[0].id;
  }

  let paymentMethod = paymentMethods[0].id;

  if (
    checkoutData.paymentMethod != null &&
    checkoutData.paymentMethod != undefined
  ) {
    paymentMethod = checkoutData.paymentMethod;
  }else{
    paymentMethod = paymentMethods[0].id;
  }
  

  const [selectedShippingMethod, setSelectedShippingMethod] = useState(shippingMethod);
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(paymentMethod);
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      $(`#p-${selectedPaymentMethod}`).prop('checked',true).trigger('change');
      $(`#s-${selectedShippingMethod}`).prop('checked',true).trigger('change');
    }, 100);
  });

  const renderShippingMethod = (handleChange, values, errors, touched) => {
    if (!checkoutData.pickup) {
      return (
        <div>
          <h5>{shipping_method}</h5>
          <p></p>
          <ul className="payment_method">
            {shippingMethods.map((method, index) => {
              let id = `s-` + method.id;
              let idHashed = `s-` + method.id;
              return (
                <li key={method.name}>
                  <div className="custom-control custom-radio">
                    <Field
                      name={name}
                      render={({ field, form }) => {
                        return (
                          <input
                            type="radio"
                            id={id}
                            name="shippingMethod"
                            className="custom-control-input"
                            value={method.id}
                            onChange={handleChange}
                            //checked={field.value}
                          />
                        );
                      }}
                    />
                    <label
                      className="d-flex custom-control-label"
                      style={{ justifyContent: "space-between" }}
                      for={idHashed}
                    >
                      <span>{method.name}</span>
                      <span class="card-link">
                        {currency}: {useCurrencyConverter(method.rate)}
                      </span>
                    </label>
                  </div>
                </li>
              );
            })}
          </ul>
          <p className="text-danger">
            {errors.shippingMethod &&
              touched.shippingMethod &&
              errors.shippingMethod}
          </p>
        </div>
      );
    }
  };

  const renderPaymentMethod = (handleChange, values, errors, touched) => {
    return (
      <div>
        <CheckoutInfo title={payment_method} subTitle={We_updates} />
        <ul className="payment_method">
          {paymentMethods.map((method, index) => {
            let id = `p-` + method.id;
            let idHashed = `p-` + method.id;
            return (
              <li key={method.slug} >
                <div class="custom-control custom-radio">
                  <Field
                    name={name}
                    render={({ field, form }) => {
                      return (
                        <input
                          type="radio"
                          id={id}
                          name="paymentMethod"
                          className="custom-control-input"
                          value={method.id}
                          //checked={field.value}
                          onChange={handleChange}
                        />
                      );
                    }}
                  />
                  <label
                      className="d-flex custom-control-label"
                      style={{ justifyContent: "space-between" }}
                      for={idHashed}
                    >
                      <span>{method.name}</span>
                      <span class="card-link">
                        &nbsp;
                      </span>
                    </label>                  
                </div>
              </li>
            );
          })}
        </ul>
        <p className="text-danger">
          {errors.paymentMethod &&
            touched.paymentMethod &&
            errors.paymentMethod}
        </p>
      </div>
    );
  };

  return (
    <div className="row setup-content" id="step-3">
      <div className="col-xs-6 ">
        <div className="col-md-12">
          <Formik
            enableReinitialize
            initialValues={{
              paymentMethod: selectedPaymentMethod,
              shippingMethod: selectedShippingMethod,
            }}
            validate={(values) => {
              const errors = {};
              if (!values.paymentMethod) {
                errors.paymentMethod = required;
              }

              if (!checkoutData.pickup && !values.shippingMethod) {
                errors.shippingMethod = required;
              }

              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                setSubmitting(false);

                checkoutData.paymentMethod = parseInt(values.paymentMethod);

                if (!checkoutData.pickup) {
                  checkoutData.shippingMethod = parseInt(values.shippingMethod);
                }

                
                dispatch(updateCheckoutData(checkoutData));

                console.log(checkoutData);

                Router.push("/revieworder");
              }, 500);
              // onSubmitApi(values)
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => {
              
              return (
                <Form>
                  {renderPaymentMethod(handleChange, values, errors, touched)}
                  {renderShippingMethod(handleChange, values, errors, touched)}
                  <button
                    className="btn btn-primary nextBtn btn-lg pull-right"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    {Review_Order}
                  </button>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </div>
  );
}
export default StepThree;
