import ProductItem from "./productItem";
import { useRouter } from "next/router";
import Products from "../home/products";
import { useState, useEffect } from "react";
import CategoryModal from "../../common/categoryModal";
import SubHeader from "../../common/subHeader";
import DetailPopUp from "../home/detailPopUp";
import useLanguage from "../../../services/useLanguage";
import { useSelector } from "react-redux";

function WhiteWrapper({ products, category_id }) {
  
  const categories = useSelector((state) => state.categories);
  const category = categories.find((c) => c.id == category_id);
  const lang = useLanguage();

  let hasChildren =
    category != undefined ? category.children.length > 0 : false;

  return (
    <div>
      <SubHeader
        title={category != undefined ? category[lang] : ""}
        hasChildren={hasChildren}
      />
      <div
        className="white_wrapper products_list2 "
        style={{ background: "none" }}
      >
        <div className="card">
          <div className="card-body">
            {products.map((product) => {
              return (
                <Products
                  product={product}
                  key={`prod-` + product.variant_id}
                />
              );
            })}
          </div>
        </div>
      </div>
      <CategoryModal categoryId={category_id} />
      <DetailPopUp />
    </div>
  );
}
export default WhiteWrapper;
