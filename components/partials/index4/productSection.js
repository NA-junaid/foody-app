import React, { useState, useEffect } from "react";

import ProductGridItem from "../home/productGridItem";
import TopCategory from "../home/topCategory";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useTranslation from "../../../services/useTranslation";
import Link from "next/link";
import FilteredProductSection from "./filteredProductSection";

function ProductSection() {
  const categories = useSelector((state) => state.categories);
  const lang = useLanguage();
  
  const { showmore } = useTranslation();

  const products = useSelector((state) => state.products);
  
  return (
    <div>
      {categories.map((category) => {
        
        return (
          <div key={`cat-sub-` + category.id}>
            <div className="d-flex mt-3 ml-3 mr-3  font-bold justify-content-between">
              <h6>{category[lang]}</h6>
              <span>
                <Link
                  href={{
                    pathname: "/moreitems",
                    query: {
                      category_id: category.id,
                    },
                  }}
                >
                  <a href="#" className="show-more">
                    {category["children"].length > 0 
                      ? showmore 
                      : 
                      products.filter((p) => {
                        return p.category_id == category.id;
                      }).length > 0 
                      ? showmore
                      :
                      ''
                    }
                 
                  </a>
                </Link>
              </span>
            </div>
            <FilteredProductSection
              category={category}
              key={category.id}
            />
          </div>
        );
      })}
    </div>
  );
}

export default ProductSection;
