import React from "react";
import useLanguage from "../../../services/useLanguage";
import Link from "next/link";

function MenuProduct({ category, products }) {
  const lang = useLanguage();
  return (
    <li>
      <Link
        href={{ pathname: "/index5_list", query: { category_id: category.id } }}
      >
        <a >
          <div className="img_block">
            <h4>{category[lang]}</h4>
            <span>{products}</span>
            <img loading="lazy" src={category.image} className="xlarge_img" />
          </div>
        </a>
      </Link>
    </li>
  );
}

export default MenuProduct;
