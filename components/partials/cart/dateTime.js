import React from "react";
import useTranslation from "../../../services/useTranslation";
import { useSelector, useDispatch } from "react-redux";

function DateTime({ delivery_time }) {
  const { time } = useTranslation();
  const checkoutData = useSelector((state) => state.checkoutData);
  const outlets = useSelector((state) => state.outlets);
  const areas = useSelector((state) => state.areas);
  const zones = useSelector((state) => state.zones);  

  const renderModalLink = () => {

    let result = true;

    if (checkoutData.pickup) {
      let outlet = outlets.find((o) => o.id == checkoutData.outletId);
      result = outlet.enable_business_hours == 1;
    }

    if(result){
      return <a data-toggle="modal" data-target="#time_popup" >{'change'}</a>
    }
  }


  return (
    <p className=" pt-3 pb-3 m-0">
      <span className="pr-3">{delivery_time}</span>
      {renderModalLink()}
      
    </p>
  );
}
export default DateTime;
