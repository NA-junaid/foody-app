import React from "react";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useCurrency from "../../../services/useCurrency";
import useCart from "../../../services/useCart";
import useTranslation from "../../../services/useTranslation";
import { useState, useEffect } from "react";

function ListItem({ product }) {
  const cart = useSelector((state) => state.cart);
  const [setValue] = useCart({});
  const lang = useLanguage();
  const currency = useCurrency();
  const { empty_cart } = useTranslation();

  const [count, setCount] = useState(product.quantity);
  const [init, setInit] = useState(false);
  const [increase, setIncrease] = useState(false);

  const increment = () => {
    setCount(count + 1);
    setIncrease(true);
  };

  const decrement = () => {
    if (count > 1) {
      setCount(count - 1);
      setIncrease(false);
    }
  };

  useEffect(() => {
    if (init) {
      setValue({ quantity: count, product: product, increment: increase });
    }
    setInit(true);
  }, [count]);

  return (
    <div className="qty_show" style={{ width: "auto" }}>
      <form>
        <button
          className="value-button minus"
          value="Decrease Value"
          type="button"
          onClick={() => decrement()}
        >
          -
        </button>
        <span className="number">{count}</span>

        <button
          className="value-button"
          value="Increase Value"
          type="button"
          onClick={() => increment()}
        >
          +
        </button>
      </form>
    </div>
  );
}

export default ListItem;
