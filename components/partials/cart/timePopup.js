import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useTranslation from "../../../services/useTranslation";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";

const getDatesBetweenDates = (startDate, endDate, lang) => {
  var days = {
    en: [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ],
    ar: [
      "الاحد",
      "الاثنين",
      "الثلاثاء",
      "الاربعاء",
      "الخميس",
      "الجمعة",
      "السبت",
    ],
  };

  let titles = { en: ["Today", "Tommorow"], ar: ["اليوم", "غدا"] };

  let dates = [];
  //to avoid modifying the original date
  const theDate = new Date(startDate);
  let index = 0;
  let type = lang == "ar" ? "ar-EG" : "default";
  while (theDate < endDate) {
    let d = new Date(theDate);
    let month = d.toLocaleString(type, { month: "short" });
    let name = `${d.getDate()} ${month}`;
    let title =
      index == 0 || index == 1 ? titles[lang][index] : days[lang][d.getDay()];
    dates = [...dates, { name: name, title: title }];
    theDate.setDate(theDate.getDate() + 1);
    index++;
  }
  return dates;
};

const getTimeIntervals = (startTime, endTime) => {
  //Parse In
  var parseIn = function (date_time) {
    var d = new Date();
    d.setHours(date_time.getHours());
    d.setMinutes(0);

    return d;
  };

  //make list
  var getTimeIntervals = function (time1, time2) {
    var arr = [];
    while (time1 < time2) {
      arr.push(time1.toTimeString().substring(0, 5));
      time1.setMinutes(time1.getMinutes() + 60);
    }
    return arr;
  };

  startTime = parseIn(startTime);
  endTime = parseIn(endTime);

  return getTimeIntervals(startTime, endTime);
};

let timeSlots = [
  { key: 1, start: "01:00", end: "02:00", visible: true },
  { key: 2, start: "02:00", end: "03:00", visible: true },
  { key: 3, start: "03:00", end: "04:00", visible: true },
  { key: 4, start: "04:00", end: "05:00", visible: true },
  { key: 5, start: "05:00", end: "06:00", visible: true },
  { key: 6, start: "06:00", end: "07:00", visible: true },
  { key: 7, start: "07:00", end: "08:00", visible: true },
  { key: 8, start: "08:00", end: "09:00", visible: true },
  { key: 9, start: "09:00", end: "10:00", visible: true },
  { key: 10, start: "10:00", end: "11:00", visible: true },
  { key: 11, start: "11:00", end: "12:00", visible: true },
  { key: 12, start: "12:00", end: "13:00", visible: true },
  { key: 13, start: "13:00", end: "14:00", visible: true },
  { key: 14, start: "14:00", end: "15:00", visible: true },
  { key: 15, start: "15:00", end: "16:00", visible: true },
  { key: 16, start: "16:00", end: "17:00", visible: true },
  { key: 17, start: "17:00", end: "18:00", visible: true },
  { key: 18, start: "18:00", end: "19:00", visible: true },
  { key: 19, start: "19:00", end: "20:00", visible: true },
  { key: 20, start: "20:00", end: "21:00", visible: true },
  { key: 21, start: "21:00", end: "22:00", visible: true },
  { key: 22, start: "22:00", end: "23:00", visible: true },
  { key: 23, start: "23:00", end: "00:00", visible: true },
  { key: 24, start: "00:00", end: "01:00", visible: true },
];

function TimePopup({ delivery_time }) {
  const lang = useLanguage();
  const [selectedDate, setSelectedDate] = useState();
  const [selectedTime, setSelectedTime] = useState();
  const [selectedType, setSelectedType] = useState(0);
  const [init, setInit] = useState(false);
  const [businessHours, setBusinessHours] = useState([]);
  const [times, setTimes] = useState(timeSlots);
  const checkoutData = useSelector((state) => state.checkoutData);
  const outlets = useSelector((state) => state.outlets);
  const areas = useSelector((state) => state.areas);
  const zones = useSelector((state) => state.zones);
  const dispatch = useDispatch();
  const { DeliveryDateTime, ScheduleOrder } = useTranslation();

  const today = new Date();
  const threedaysFromNow = new Date(today);
  threedaysFromNow.setDate(threedaysFromNow.getDate() + 7);

  let dates = getDatesBetweenDates(today, threedaysFromNow, lang);

  const initialize = () => {
    let businessHours = {};
    if (checkoutData.pickupOrDeliverySelected) {
      if (checkoutData.pickup) {
        let outlet = outlets.find((o) => o.id == checkoutData.outletId);
        businessHours = JSON.parse(outlet.business_hours, false);
      } else {
        let area = areas.find((a) => a.name == checkoutData.area);
        let zone = zones.find((z) => z.id == area.zone_id);
        businessHours = JSON.parse(zone.business_hours, false);
      }

      let bh = [];

      let days = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
      ];

      for (const property in businessHours) {
        if (businessHours[property].hasOwnProperty("name")) {
          if (
            businessHours[property].slot_start != null &&
            businessHours[property].slot_end != null
          ) {
            let start = businessHours[property].slot_start;
            let end = businessHours[property].slot_end;
            let start_parts = start.split(":");
            let end_parts = end.split(":");
            let date = new Date();
            let start_date = new Date(date.setHours(parseInt(start_parts)));
            let end_date = new Date(date.setHours(parseInt(end_parts)));

            let dayName = days[date.getDay()];
            let name = businessHours[property].name;
            let nextDay = days[date.getDay() + 1];

            if (dayName.toLowerCase() == businessHours[property].name) {
              name = "today";
            } else if (nextDay.toLowerCase() == businessHours[property].name) {
              name = "tommorow";
            }

            let a = getTimeIntervals(start_date, end_date);
            let obj = { day: name, slots: a };

            bh.push(obj);
          } else {
            let date = new Date();
            let dayName = days[date.getDay()];
            let name = businessHours[property].name;
            let nextDay = days[date.getDay() + 1];

            if (dayName.toLowerCase() == businessHours[property].name) {
              name = "today";
            } else if (nextDay.toLowerCase() == businessHours[property].name) {
              name = "tommorow";
            }
            let obj = { day: name, slots: [] };
            bh.push(obj);
          }
        }
      }

      setBusinessHours(bh);
    }
  };

  useEffect(() => {
    $("#below-button").show();
    if (!init) {
      initialize();
      setInit(true);
    }
  }, [init, setSelectedDate]);

  const reRenderTimeSlots = (i) => {
    setSelectedDate(i);

    $(".active").removeClass("highlight");
    $(`li[data-tag='${i.title}']`).addClass("highlight");

    let bh = businessHours.find((b) => b.day == i.title.toLowerCase());

    let d = new Date();

    let hours = d.getHours();

    times.map((t) => {
      if (bh != undefined) {
        if (bh.slots.length > 0) {
          t.visible = bh.slots.includes(t.start);
        } else {
          t.visible = true;
        }

        if (bh.day == "today") {
          let parts = t.start.split(":");
          if (parseInt(parts[0]) > hours) {
            t.visible = true;
          } else {
            t.visible = false;
          }
        }
      } else {
        t.visible = true;
      }

      return t;
    });

    setTimes(times);
    let first = times.find((i) => i.visible);
    if (first != undefined) {
      setSelectedTime(`${first.start} - ${first.end}`);
    }
  };

  const renderTags = (days) => {
    return days.map((i, index) => {
      return (
        <li className="active" keys={`te-${index}`} data-tag={i.title}>
          <a
            className="d-flex flex-column justify-content-between text-center"
            onClick={() => reRenderTimeSlots(i)}
          >
            <span>{i.name}</span>
            <span>
              <b>{i.title}</b>
            </span>
          </a>
        </li>
      );
    });
  };

  const renderTimes = () => {
    return times.map((t) => {
      let tm = `time-${t.key}`;

      if (t.visible) {
        return (
          <div
            className="p-3"
            onClick={() => setSelectedTime(`${t.start} - ${t.end}`)}
            key={tm}
          >
            <div className="custom-control custom-radio">
              <input
                type="radio"
                className="custom-control-input"
                id={tm}
                name="time"
                value={`${t.start} - ${t.end}`}
                checked={`${t.start} - ${t.end}` == selectedTime}
                onChange={() => console.log("done")}
              />
              <label className="custom-control-label" htmlFor={tm}>
                {`${t.start} - ${t.end}`}
              </label>
            </div>
          </div>
        );
      }
    });
  };

  const renderScheduleContent = () => {
    if (selectedType == 1) {
      return (
        <div>
          <ul className="scrollspyArea pt-3">{renderTags(dates)}</ul>
          <hr />
          {renderTimes()}
        </div>
      );
    }
  };

  const setCustomerTime = (val) => {
    setSelectedType(val);
    if (val == 1) {
      setSelectedDate(dates[0]);
      setSelectedTime(times[0]);
      reRenderTimeSlots(dates[0]);
      setTimeout(() => {
        $(".active").removeClass("highlight");
        $(`li[data-tag='Today']`).addClass("highlight");
      },100)
    }
  };

  const setDeliveryTime = () => {
    checkoutData.deliveryTime = "";

    if (selectedType == 1) {
      checkoutData.deliveryTime = `${selectedDate.name}${" "}${selectedTime}`;
      checkoutData.isScheduled = true;
    } else {
      checkoutData.deliveryTime = delivery_time;
      checkoutData.isScheduled = false;
    }

    dispatch(updateCheckoutData(checkoutData));
    $("#time_popup").modal("hide");
  };

  const renderContent = () => {
    return (
      <div className="modal-body" style={{ minHeight: "600px" }}>
        <div className="product_detail">
          <div
            className="d-flex p-3 font-bold schedule-header"
          >
            <h3
              className="text-center"
              style={{ color: "#FFF", marginTop: "8px" }}
            >
              {DeliveryDateTime}
            </h3>
          </div>
          <hr />
          <div className="extra_selection">
            <div className="card mb-0">
              <div className="p-3">
                <div className="custom-control custom-radio">
                  <input
                    type="radio"
                    className="custom-control-input"
                    id="delivery-time"
                    name="a"
                    onChange={() => setCustomerTime(0)}
                    checked={selectedType == 0}
                    value={0}
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="delivery-time"
                  >
                    {delivery_time}
                  </label>
                </div>
              </div>
              <div className="p-3">
                <div className="custom-control custom-radio">
                  <input
                    type="radio"
                    className="custom-control-input"
                    id="schedule-order"
                    name="a"
                    onChange={() => setCustomerTime(1)}
                    checked={selectedType == 1}
                    value={1}
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="schedule-order"
                  >
                    {ScheduleOrder}
                  </label>
                </div>
              </div>
            </div>
            <hr />
            {renderScheduleContent()}
          </div>
        </div>
        <div className="row setup-content" id="below-button">
          <div className="col-md-12">
            <button
              className="btn btn-primary nextBtn proceedBtn btn-lg text-center"
              type="button"
              style={{ margin: "0 15px" }}
              onClick={() => setDeliveryTime()}
            >
              <span>Set time: </span>
              <span>{renderSchedule()}</span>
            </button>
          </div>
        </div>
      </div>
    );
  };

  const renderSchedule = () => {
    if (selectedType == 1) {
      return `${selectedDate != undefined ? selectedDate.title : ""}${" "}${
        selectedTime != undefined ? selectedTime : ""
      }`;
    } else {
      return delivery_time;
    }
  };

  return (
    <div
      className="modal fade item_detail_popup"
      id="time_popup"
      tabIndex={-1}
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog" role="document">
        <div className="modal-content">
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
            style={{ marginTop: "10px" }}
          >
            <span aria-hidden="true">×</span>
          </button>

          {renderContent()}
        </div>
      </div>
    </div>
  );
}

export default TimePopup;
