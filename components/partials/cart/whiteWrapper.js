import React, { useState, useEffect } from "react";
import CartListItem from "./cartListItems";
import DateTime from "./dateTime";
import PromotionCode from "./promotionCode";
import TotalSummary from "./totalSummary";
import useTranslation from "../../../services/useTranslation";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import Router from "next/router";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";
import TimePopup from "./timePopup";

const EstimatedTime = ({ delivery_time }) => {
  const checkoutData = useSelector((state) => state.checkoutData);
  const { EstimatedTime, ScheduledAt } = useTranslation();

  let title = checkoutData.isScheduled ? ScheduledAt : EstimatedTime;

  return (
    <span>
      <hr />
      <span className="d-flex justify-content-between">
        <span className=" pt-3 pb-3 m-0">{title}</span>

        <DateTime delivery_time={checkoutData.deliveryTime} />
      </span>
    </span>
  );
};

function WhiteWrapper() {
  const {
    Order_Items,
    empty_cart,
    checkout,
    min_order_amount_of,
    home,
    cart,
  } = useTranslation();
  const userCart = useSelector((state) => state.cart);
  const webSetting = useSelector((state) => state.webSetting);

  const checkoutData = useSelector((state) => state.checkoutData);
  const areas = useSelector((state) => state.areas);
  const zones = useSelector((state) => state.zones);
  const dispatch = useDispatch();

  const [update, setUpdate] = useState();
  let [result, setResulte] = useState();
  let [selected_area, setSelectedArea] = useState();

  const initialize = () => {
    setUpdate(false);
    if (checkoutData.pickup == true) {
      result = null;
    } else {
      if (checkoutData.area != null) {
        selected_area = areas.find((area) => area.name == checkoutData.area);

        result = zones.find((zone) => zone.id == selected_area.zone_id);
        setUpdate(true);
        setSelectedArea(selected_area);
        setResulte(result);
        checkoutData.outletId = result.outlet_id;
        checkoutData.shippingRate = result.delivery_charges;
        checkoutData.deliveryTime = result.delivery_time;
      }
    }
    dispatch(updateCheckoutData(checkoutData));
  };

  useEffect(() => {
    if (!update) {
      initialize();
    }
  }, [false]);

  const renderCheckoutButton = () => {
    if (
      result != null &&
      parseFloat(result.min_order) > parseFloat(userCart.total)
    ) {
      return (
        <button
          style={{ width: "100%", "z-index": "1" }}
          className="btn btn-danger nextBtn btn-lg pull-right mt-4 mb-2"
        >
          {min_order_amount_of} {parseFloat(result.min_order)}{" "}
          {webSetting.default_currency}
        </button>
      );
    } else {
      return (
        <div className="row setup-content">
          <div className="col-md-12">
            <button
              className="btn btn-primary nextBtn proceedBtn btn-lg pull-right"
              type="button"
              onClick={() => Router.push("/checkout")}
            >
              {checkout}
            </button>
          </div>
        </div>
      );
    }
  };
  const renderCart = () => {
    if (userCart.items.length > 0) {
      return (
        <div className="col-md-12">
          <h2 className="text-uppercase pt-3 pb-3">{Order_Items}</h2>
          <hr />
          <CartListItem />
          <EstimatedTime
            delivery_time={result != undefined ? result.delivery_time : ""}
          />
          <hr />
          <PromotionCode />
          <TotalSummary />
          {renderCheckoutButton()}
        </div>
      );
    } else {
      return (
        <div className="col-md-12">
          <img loading="lazy" src="/assets/img/bag.svg" alt="cart" />
          <h2 className="text-uppercase pt-3 pb-3">
            <Link href="/">
              <a href="#">
                <br />
                {empty_cart}
              </a>
            </Link>
          </h2>
        </div>
      );
    }
  };
  return (
    <div className="white_wrapper cart">
      <div className="container">
        <div className="row">
          <div className="col-md-12 mt-2 ">
            <nav aria-label="breadcrumb ">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link href="/">
                    <a>{home}</a>
                  </Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  {cart}
                </li>
              </ol>
            </nav>
          </div>
        </div>
        <div className="row">{renderCart()}</div>
      </div>
      <TimePopup delivery_time={result != null ? result.delivery_time : ""} />
    </div>
  );
}
export default WhiteWrapper;
