import React, { useState, useEffect } from "react";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";

import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";

function PromotionCode() {
    const store = useSelector((state) => state.store);
    const { promotioncode, enterpromotioncode, apply } = useTranslation();
    const plugins = useSelector((state) => state.plugins);
    
    const checkoutData = useSelector((state) => state.checkoutData);
    let [dis_type, setDisType] = useState()
    let [cop_discount, setDisCop] = useState()
    let [discount_voucher_code, setDisVou] = useState()

    let dis_plug = null;
    checkoutData.dis_type = '';
    checkoutData.cop_discount = '';
    checkoutData.discount_voucher_code ='';

    // checkoutData.dis_type = 'amount';
    // checkoutData.cop_discount = (5.234234).toFixed(store.round_off);
    $('#discount_voucher').val(checkoutData.discount_voucher_code);
    const handleClick = () => {
        if($('#discount_voucher').val() == ''){
            toastr.error(enterpromotioncode);
            return;
        }


        let plugi = dis_plug;
        let voucher = $('#discount_voucher').val();
        
        checkoutData.discount_voucher_code = voucher;
        if(voucher.length > 0) {

            let url = plugi.api_url;
            let token = plugi.api_token;
            var settings = {
                "url": url + "vouchers/verify/" + voucher,                                  
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": "Bearer " + token,
                    "Accept": "application/json"
                },
            };

            let currency = 'KWD';

            $.ajax(settings).done(function (response) {
                
                if(response.data == null) {
                    $('.voucher-error').html('Invalid coupon');
                    $('.voucher-error').removeClass('d-none');
                    $('.voucher-error').removeClass('text-success');
                    $('.voucher-error').addClass('text-danger');
                    checkoutData.dis_type = '';
                    checkoutData.cop_discount = '';
                }
                if(response.status == 'valid') {
                    $('.voucher-error').html('Coupon Applied');
                    $('.voucher-error').removeClass('d-none');
                    $('.voucher-error').removeClass('text-danger');
                    $('.voucher-error').removeClass('text-warning');
                    $('.voucher-error').addClass('text-success');
                    $('#discount_voucher_code').val(voucher);
                    $('.discount_price').removeClass('d-none');
                    $('.discount_price_val').removeClass('d-none');
                    if(response.data.discount_type == 1) {
                        checkoutData.dis_type = 'amount';

                        checkoutData.cop_discount = response.data.discount_amount.toFixed(store.round_off);

                    } else if(response.data.discount_type == 2) {
                        checkoutData.dis_type = 'percentage';
                        checkoutData.cop_discount = response.data.discount_amount.toFixed(store.round_off)
                    }

                } else if(response.status == 'expired') {
                    $('.voucher-error').html('Coupon expired');
                    $('.voucher-error').removeClass('text-danger');
                    $('.voucher-error').removeClass('text-success');
                    $('.voucher-error').addClass('text-warning');
                    $('#discount_voucher_code').val();
                    checkoutData.dis_type = '';
                    checkoutData.cop_discount = '';
                    checkoutData.discount_voucher_code = '';
                } else if(response.status == 'limit_reached') {
                    checkoutData.dis_type = '';
                    checkoutData.cop_discount = '';
                    checkoutData.discount_voucher_code = '';
                    $('.voucher-error').html('Coupon usage limit reached');
                    $('.voucher-error').removeClass('text-danger');
                    $('.voucher-error').removeClass('text-success');
                    $('.voucher-error').addClass('text-warning');
                    $('#discount_voucher_code').val();
                }
            }).fail(function (jqXHR, textStatus) {
                
            });
        }

    }

    useEffect(() => {
        setDisVou(checkoutData.discount_voucher_code );
        setDisType(checkoutData.dis_type );
        setDisCop(checkoutData.cop_discount );
      },[checkoutData]);

    const renderDiscountCode = () => {
        let is_true = false;
        dis_plug = null;
        plugins.map((plugin) => {
            if(plugin.plugin != null && plugin.plugin.slug == 'discount'){
                is_true = true;
                dis_plug = plugin;
            }
        })
        if(is_true){
            return (
                <div className="">
                  <h2 className="text-uppercase pt-4 pb-3">Promotion Code</h2>
                  <div className="form-row">
                    <div className="col-auto">
                      <input
                        id="discount_voucher"
                        type="text"
                        placeholder="Enter Promotion Code"
                        className="form-control col-xs-8"
                        name=""
                        required
                      />
                    </div>
                    <div className="col-auto ">
                      <button
                        className="btn btn-primary apply_btn pull-right col-xs-4 m-1"
                        type="button"
                        onClick={() => handleClick()}
                        style={{
                          background: "#2D2F41",
                          backgroundColor: "#2D2F41",
                          borderColor: "#2D2F41",
                        }}
                      >
                        {apply}
                      </button>
                    </div>
                    <span className="d-none error text-danger voucher-error"></span>
                  </div>
                </div>
            );
        }
    };

    return (
        <div>
            {renderDiscountCode()}
        </div>
    );
}
export default PromotionCode;
