import React, { useState, useEffect } from "react";
import { useSelector,useDispatch } from "react-redux";
import useTranslation from "../../../services/useTranslation";
import useCurrency from "../../../services/useCurrency";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";
import urlService from "../../../services/urlService";
import networkService from "../../../services/networkService";
import useCurrencyConverter from "../../../services/useCurrencyConverter";

function TotalSummary({delivery_charges}) {
  const { sub_total, Delivery, total, discount } = useTranslation();
  const cart = useSelector((state) => state.cart);
  const store = useSelector((state) => state.store);
  const checkoutData = useSelector((state) => state.checkoutData);
  const currency = useCurrency();
  
  const [cartTotal, setCartTotal] = useState(cart.total);
  let [cop_discount_amount, setDisCopAmount] = useState();
  checkoutData.cop_discount_amount = '';
  let is_discount = false;

  if (checkoutData.cop_discount > 0) {
    is_discount = true;
  } else {
    checkoutData.cop_discount = 0;
  }
  const dispatch = useDispatch();
  const [update,setUpdate] = useState();
  const plugins = useSelector((state) => state.plugins);

  let [reward_points,setRewardPoints] = useState();
  let [reward_amount,setRewardAmount] = useState();
  let [dis_plug,setDisPlug] = useState();
  
  const applyReward = () => {
    if($('#applyReward').is(':checked')){
      if(dis_plug != undefined && dis_plug != null && reward_points > 0){
        reward_amount = (reward_points * (dis_plug.redeem_rate / dis_plug.points)).toFixed(store.round_off)
        setRewardAmount(reward_amount)
        checkoutData.loyalty_reward_amount = reward_amount;
        let t2 = parseFloat(cart.total) - parseFloat(reward_amount);
        setCartTotal(t2.toFixed(store.round_off));

        // console.log(cartTotal, reward_points * (dis_plug.redeem_rate / dis_plug.points),(dis_plug.redeem_rate / dis_plug.points), reward_points , dis_plug.redeem_rate );
      }else{
        reward_amount = 0;
        setRewardAmount(reward_amount)
        checkoutData.loyalty_reward_amount = 0;
      }
      dispatch(updateCheckoutData(checkoutData));
    }else{
      checkoutData.loyalty_reward_amount = 0;
      let t3 = parseFloat(cart.total);
      setCartTotal(t3.toFixed(store.round_off));
    }
    dispatch(updateCheckoutData(checkoutData));
  }
  let run =false;
  const getRewardPoints = async () => {
   
    if(run == false){
    const response = await networkService.get(urlService.getAccountEdit);
      if (response != null && response.IsValid == true){
        const new_response = await networkService.get(urlService.getLoyaltyPoints+response.Payload.customer_id);
        if (new_response != null && new_response.IsValid == true){
          console.log(new_response.Payload); 
          reward_points = new_response.Payload;
          setRewardPoints(reward_points);
              initialize()
              run = true;
        }else{
           reward_points = 0;
          setRewardPoints(reward_points);
        }
      }else{
         reward_points = 0;
        setRewardPoints(reward_points);
      }
    }


    // const response = await networkService.get(urlService.getreward);
    // if (response.IsValid) {
    //   setOrders(response.Payload.data);
    //   setInit(true);
    // }


   
  }
  const initialize = () => {
    if($('#applyReward').is(':checked')){
    }else{
      reward_amount = 0;
      setRewardAmount(reward_amount)
      checkoutData.loyalty_reward_amount = 0;
      dispatch(updateCheckoutData(checkoutData));
    }
    plugins.map((plugin) => {
        if(plugin.plugin != null && plugin.plugin.slug == 'loyalty'){
            if(plugin.plugin.loyalty != null){
              dis_plug = plugin.plugin.loyalty;
              setDisPlug(dis_plug);
              getRewardPoints();
            }            
        }
    })

    setUpdate(true);
    // console.log(checkoutData,delivery_charges);
    let t = cartTotal;
    if (checkoutData.dis_type != null && checkoutData.dis_type == "amount") {
      checkoutData.cop_discount_amount = parseFloat(checkoutData.cop_discount);
      t = parseFloat(cart.total) - parseFloat(checkoutData.cop_discount);
      setCartTotal(t.toFixed(store.round_off));
    }
    if (checkoutData.dis_type != null && checkoutData.dis_type == "percentage") {
      t = (parseFloat(cart.total) / 100) * parseFloat(checkoutData.cop_discount);
      checkoutData.cop_discount_amount = t.toFixed(store.round_off);
      let t1 = parseFloat(cart.total) - t;
      setCartTotal(t1.toFixed(store.round_off));
    }

    if (delivery_charges != undefined && parseFloat(delivery_charges) > 0) {
      t = parseFloat(cart.total) + (parseFloat(delivery_charges));
      setCartTotal(t.toFixed(store.round_off));
    }
    if(dis_plug != undefined && dis_plug != null && reward_points > 0){
      reward_amount = (reward_points * (dis_plug.redeem_rate / dis_plug.points)).toFixed(store.round_off)
      setRewardAmount(reward_amount)
      // console.log(cartTotal, reward_points * (dis_plug.redeem_rate / dis_plug.points),(dis_plug.redeem_rate / dis_plug.points), reward_points , dis_plug.redeem_rate );
    }else{
      reward_amount = 0;
      setRewardAmount(reward_amount)
    }
    dispatch(updateCheckoutData(checkoutData));
  }
  
  useEffect(() => {
    if(!update){
      initialize();
    }
  },[false]);

  useEffect(() => {
    setCartTotal(cart.total);
    initialize();
  },[cart.total]);  

  const renderShippingTitle = () => {
      return (
        <>
          {delivery_charges > 0 ? <p>{Delivery}</p> : ''}
          {reward_amount > 0 ? <p>Reward Amount</p> : ''}
          {reward_amount > 0 ? <p><br/></p> : ''}
          {checkoutData.cop_discount > 0 ? <p>{discount}</p> : ""}
        </>
      );
  };

  const renderShipping = () => {
    console.log(checkoutData);
      return (
        <>
          {delivery_charges > 0 ?  <p>
            {currency} {delivery_charges.toFixed(store.round_off)}
          </p> : ''}
          {/*<p>{delivery_charges}</p>*/}
          {reward_amount > 0 ? <p> {currency} - {useCurrencyConverter(reward_amount)}</p> : ''}
          {reward_amount > 0 ? <p> 
              <div className="form-check form-check-inline">
                <input type="checkbox" className="form-check-input" id="applyReward" onClick={() => applyReward()} />
                <label className="form-check-label" htmlFor="applyReward">
                  Apply
                </label>
              </div>
            </p> : ''}
          {checkoutData.cop_discount > 0 ? (
            <p>
              {" "}
              {currency} - {useCurrencyConverter(checkoutData.cop_discount_amount)}
            </p>
          ) : (
            ""
          )}
        </>
      );
  };

  return (
    <div>
      <div className="d-flex p-4 grey mt-2 mb-5 justify-content-between cart_total">
        <span>
          <p>{sub_total}</p>
          {renderShippingTitle()}
          <p>
            <b>{total}</b>
          </p>
        </span>
        <span className="text-right">
          <p>
            {currency} {useCurrencyConverter(cart.total)}
          </p>
          {renderShipping()}
          <p>
            <b>
              {currency} {useCurrencyConverter(cartTotal)}
            </b>
          </p>
        </span>
      </div>
    </div>
  );
}

export default TotalSummary;
