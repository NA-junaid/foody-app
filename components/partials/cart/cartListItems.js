import React from "react";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useCurrency from "../../../services/useCurrency";
import useCart from "../../../services/useCart";
import useTranslation from "../../../services/useTranslation";
import { useState, useEffect } from "react";
import ListItem from "./listItem";
import useCurrencyConverter from "../../../services/useCurrencyConverter";

function CartListItem() {
  const cart = useSelector((state) => state.cart);
  const [setValue] = useCart({});
  const lang = useLanguage();
  const currency = useCurrency();

  const removeItem = (product) => {
    setValue({ quantity: 0, product: product, increment: false });
  };
  

  return (
    <ul className="cart_list_items pt-2 pb-2">
      {cart.items.map((product) => {
        return (
          <li key={product.variant_id} className="d-flex flex-column">
            <div>
              <span className="">{product.total_variants > 1 ? product.selected_variant != null ?  product.selected_variant :product[lang].name  : product[lang].name}</span>
            </div>
            <div className="d-flex justify-content-between">
              <ListItem product={product} />
              <strong style={{ paddingTop: "11px" }}>
                {currency} : {useCurrencyConverter(product.total_retail)}
              </strong>
              <span
                className="x_closer"
                onClick={() => removeItem(product)}
                style={{ paddingTop: "7px" }}
              >
                X
              </span>
            </div>
          </li>
        );
      })}
    </ul>
  );
}
export default CartListItem;
