import React from "react";
import DeliveryInfo from "./deliveryInfo";
import useTranslation from "../../../services/useTranslation";


function SpecialNotes() {
    const {special_notes} = useTranslation();
    return (
        <div>
            <DeliveryInfo title= {special_notes} />
            <p className="mt-3">
                Eius interdum officiis ullam perferendis mi ipsam eleifend eum id
                dolorum, integer? Saepe sagittis, sint exercitation cillum optio
                ultricies mollis ligula! Sunt.
          </p>
        </div>
    );
}

export default SpecialNotes;
