import React, { useState, useEffect } from "react";
import useTranslation from "../../../services/useTranslation";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import useCurrency from "../../../services/useCurrency";
import { useSelector, useDispatch } from "react-redux";
import useCurrencyConverter from "../../../services/useCurrencyConverter";

function CartTotal() {
  const checkoutData = useSelector((state) => state.checkoutData);
  const { sub_total, Delivery, total, discount } = useTranslation();
  const cart = useSelector((state) => state.cart);
  const store = useSelector((state) => state.store);
  const currency = useCurrency();
  const [cartTotal, setCartTotal] = useState(cart.total);

  let is_discount = false;
  let cop_discount = checkoutData.cop_discount_ammount;

  if(cop_discount > 0){
      is_discount = true;
  }else{
      cop_discount = 0;
  }
  

  const renderShippingTitle = () => {
    if (checkoutData.shippingRate != null) {
      return (
        <>
          <p>{Delivery}</p>
           {checkoutData.deliveryTime  ? <p>{"Time"}</p> : ''}
           {checkoutData.loyalty_reward_amount > 0 ? <p>Reward Amount</p> : ''}
          {cop_discount > 0 ? <p>{discount}</p> : '' }
        </>
      );
    }
  };

  const renderShipping = () => {
    if (checkoutData.shippingRate != null) {
      return (
        <>
          <p>
            {currency} {useCurrencyConverter(parseFloat(checkoutData.shippingRate))}
          </p>
          {checkoutData.deliveryTime ? <p>{checkoutData.deliveryTime}</p> : ''} 
          {checkoutData.loyalty_reward_amount > 0 ? <p> {currency} - {useCurrencyConverter(checkoutData.loyalty_reward_amount)}</p> : ''}
          {cop_discount > 0 ? <p> {currency} - {useCurrencyConverter(cop_discount)}</p> : '' }
        </>
      );
    }
  };

  const handleClick = () => {
      
  }

  useEffect(() => {
    let t = cartTotal;
    if (checkoutData.shippingRate != null) {
      let t = parseFloat(cart.total) + parseFloat(checkoutData.shippingRate) - parseFloat(cop_discount);
      setCartTotal(t.toFixed(store.round_off));
    }
    if (parseFloat(checkoutData.loyalty_reward_amount) > 0) {
      let t1 = parseFloat(cart.total) - checkoutData.loyalty_reward_amount;
      setCartTotal(t1.toFixed(store.round_off));
    }
  });

  return (
    <div>
      <div className="d-flex p-4 grey mt-2 mb-5 justify-content-between cart_total">
        <span>
          <p>{sub_total}</p>
          {renderShippingTitle()}
          <p>
            <b>{total}</b>
          </p>
        </span>
        <span className="text-right">
          <p>
            {currency} {useCurrencyConverter(cart.total)}
          </p>
          {renderShipping()}
          <p>
            <b>
              {currency} {useCurrencyConverter(cartTotal)}
            </b>
          </p>
        </span>
      </div>
    </div>
  );
}

export default CartTotal;
