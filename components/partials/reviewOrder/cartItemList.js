import React from "react";
import CartItemName from "./cartItemName";
import Quantity from "./quantity";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useCurrencyConverter from "../../../services/useCurrencyConverter";

function CartItemList() {
  const cart = useSelector((state) => state.cart);
  const lang = useLanguage();
  return (
    <ul className="cart_list_items pt-2 pb-2 check">
      {cart.items.map((item) => {
    console.log(item);
        
        return (
          <li>
            <CartItemName name={item.total_variants > 1 ? item.selected_variant != null ?  item.selected_variant :item[lang].name  : item[lang].name} />
            <Quantity qty={item.quantity} ttl={useCurrencyConverter(item.total)} />
          </li>
        );
      })}
    </ul>
  );
}

export default CartItemList;
