import React from "react";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import useCart from "../../../services/useCart";

function ShowQuantity({ product }) {
  const [init, setInit] = useState(false);
  const [increase, setIncrease] = useState(false);
  const cart = useSelector((state) => state.cart);
  let item = cart.items.find(
    (i) =>
      i.product_id == product.product_id && i.variant_id == product.variant_id
  );

  const quantity = item != undefined ? item.quantity : 0;
  
  const [count, setCount] = useState(quantity);
  const [setValue] = useCart({});

  const increment = () => {
    setCount(count + 1);
    setIncrease(true);
  };

  const decrement = () => {
    setCount(count > 0 ? count - 1 : count);
    setIncrease(false);
  };

  useEffect(() => {
    if(init){
      product.addons = [];
      setValue({ quantity: count, product: product, increment: increase });
    }
    setInit(true)
  }, [count, increase]);

  return (
    <div className="qty_show">
      <form>
        <button
          className="value-button minus"
          id="decrease"
          onClick={decrement}
          value="Decrease Value"
          type="button"
        >
          -
        </button>
        <input
          type="number"
          id="number"
          className="number"
          value={count}
          disabled="disabled"
        />
        <button
          className="value-button"
          id="increase"
          onClick={increment}
          value="Increase Value"
          type="button"
        >
          +
        </button>
      </form>
    </div>
  );
}

export default ShowQuantity;
