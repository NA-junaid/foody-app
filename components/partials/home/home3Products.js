import CategorySection from "../index3/categorySection";

function Home3Products({ products }) {
  return (
    <div className="products_area">
      <div id="accordion" className="accordion  mt-4 mb-4">
        <div className="card mb-0">
          <CategorySection parent={`#accordion`} products={products} />
        </div>
      </div>
    </div>
  );
}
export default Home3Products;
