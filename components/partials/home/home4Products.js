import ProductSection from "../index4/productSection";

function Home4Products() {
  return (
    <div className="home2_products_area">
      <div className="products_row">
        <ProductSection />
        <hr />
      </div>
    </div>
  );
}
export default Home4Products;
