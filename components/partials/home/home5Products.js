import MenuProduct from "../index5/menuProduct";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";

function Home5Products() {
  const { Menu } = useTranslation();
  const categories = useSelector((state) => state.categories);
  const products = useSelector((state) => state.products);

  return (
    <div className="home3_products_area">
      <h3 className="p-3">{Menu}</h3>
      <ul className="inner_pro">

        {
          categories.map(category => {
            let filteredProducts = products.filter(prod => {
              return prod.category_id == category.id;
            });
            if (filteredProducts != null && filteredProducts.length > 0) {
              return (
                <MenuProduct category={category} products={filteredProducts.length} key={category.id}  />
              )
            }
          })
        }

      </ul>
    </div>
  );
}
export default Home5Products;