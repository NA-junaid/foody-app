import React from "react";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";

function
    TabList() { 
    const areas = useSelector((state) => state.areas);
    const outlets = useSelector((state) => state.outlets);
    const { Delivery , pickup } = useTranslation();
    
    return (
        <div>
            <ul className="nav nav-pills nav-fill" id="pills-tab" role="tablist">
                {areas.length > 0 ? <li className="nav-item">
                    <a
                        className="nav-link"
                        id="pills-home-tab"
                        data-toggle="pill"
                        href="#pills-home"
                        role="tab"
                        aria-controls="pills-home"
                        aria-selected="false"
                    >
                        {Delivery}
                    </a>
                </li> : null}
                {outlets.length > 0 ? <li className="nav-item">
                    <a
                        className="nav-link"
                        id="pills-profile-tab"
                        data-toggle="pill"
                        href="#pills-profile"
                        role="tab"
                        aria-controls="pills-profile"
                        aria-selected="false"
                    >
                        {pickup}
              </a>
                </li> : null}
            </ul>
        </div>
    );
}

export default TabList;
