import React from "react";
import useCurrency from "../../../services/useCurrency";
import { useSelector} from "react-redux";
import useCurrencyConverter from "../../../services/useCurrencyConverter";

function ItemDescription({ name, price }) {
    const currency = useCurrency();
    const store = useSelector((state) => state.store);
    return (
        <div className="item_d">
            <p>{name}</p>
            <p>{`${currency} ${useCurrencyConverter(parseFloat(price))}`}</p>
        </div>
    );
}

export default ItemDescription;
