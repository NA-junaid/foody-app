import React from "react";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";

function PickUp() {
  const checkoutData = useSelector((state) => state.checkoutData);

  const { pickup } = new useTranslation();
  const pickupPopUP = () => {
    setTimeout(function () {
      $("#pills-profile-tab").trigger("click");
    }, 100);
  };

  return (
    <div className="custom-control custom-radio">
      <input
        type="radio"
        id="customRadio2"
        name="pickup"
        onClick={pickupPopUP}
        className="custom-control-input"
        checked={checkoutData.pickupOrDeliverySelected && checkoutData.pickup}
        onChange={() => console.log("Pickup")}
      />
      <label
        className="custom-control-label"
        htmlFor="customRadio2"
        data-toggle="modal"
        data-target="#pickup_opup"
      >
        {pickup}
      </label>
    </div>
  );
}

export default PickUp;
