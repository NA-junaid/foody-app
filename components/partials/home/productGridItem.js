import React from "react";
import ImageBlock from "./imageBlock";
import ItemDescription from "./itemDescription";
import useLanguage from "../../../services/useLanguage";
import useTranslation from "../../../services/useTranslation";
import { setPopup } from "../../../redux/actions/popupAction";
import { discardAddons } from "../../../redux/actions/addonAction";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ShowQuantity from "./showQuantity";

function ProductGridItem({ data }) {
  const lang = useLanguage();
  const { showmore } = useTranslation();
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();
  const toggle = () => setShow((o) => !o);

  const setPopupData = (product) => {
    dispatch(setPopup(product));
    dispatch(discardAddons());
  };

  const renderControl = (product) => {
    if (product[lang].addons.length > 0 || product.total_variants > 1) {
      return (
        <h4
          data-toggle="modal"
          data-target="#item_detail_popup"
          onClick={() => setPopupData(product)}
        >
          {showmore}
        </h4>
      );
    } else {
      return (
        <a title="Cart" onClick={toggle}>
          <img
            loading="lazy"
            src="/assets/img/cart_icon.svg"
            style={{ width: "29px", height: "29px" }}
          />
        </a>
      );
    }
  };

  return (
    <li>
        <ImageBlock img={data.image} />
        <ItemDescription name={data[lang].name} price={data.retail_price} />

        <span className="show_cart">{renderControl(data)}</span>
        {show ? <ShowQuantity product={data} /> : ""}
    </li>
  );
}

export default ProductGridItem;
