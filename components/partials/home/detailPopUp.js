import React, { useState, useEffect } from "react";
import CloseButton from "./closeButton";
import { useSelector, useDispatch } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useTranslation from "../../../services/useTranslation";
import useCurrency from "../../../services/useCurrency";
import ReactHtmlParser from "react-html-parser";
import SelectAddOn from "./selectAddOn";
import useCart from "../../../services/useCart";
import { removePopup } from "../../../redux/actions/popupAction";
import useCurrencyConverter from "../../../services/useCurrencyConverter";
import { setPopup } from "../../../redux/actions/popupAction";

function DetailPopUp() {
  const product = useSelector((state) => state.popup);
  const addons = useSelector((state) => state.addons);
  const store = useSelector((state) => state.store);
  const lang = useLanguage();
  const [count, setCount] = useState(1);
  const [total, setTotal] = useState(0);
  let [tags, setTags] = useState({attribute_value_1:'',attribute_value_2:'',attribute_value_3:''});
  const [increase, setIncrease] = useState(false);
  const currency = useCurrency();
  const [setValue] = useCart({});
  const dispatch = useDispatch();
  const { complete_selection, Error, select_quantity } = useTranslation();

  const increment = () => {
    setCount(count + 1);
    setIncrease(true);
  };

  const decrement = () => {
    setCount(count > 0 ? count - 1 : count);
    setIncrease(false);
  };

  const addToCart = () => {
    let errors = 0;

    if(count == 0){
      toastr.error(select_quantity, Error);
      return;
    }

    product[lang].addons.map((addon) => {
      let items = addons.filter((a) => a.grounName == addon.name);
      if (items.length < addon.min) {
        errors++;
      }
    });

    if (errors > 0) {
      toastr.error(complete_selection, Error);
    } else {
      product.addons = addons;

      setTimeout(() => {
        for (let i = 1; i <= count; i++) {
          setValue({ quantity: i, product: product, increment: true });
        }
      }, 500);

      setCount(1);
      dispatch(removePopup());
      tags = {attribute_value_1:'',attribute_value_2:'',attribute_value_3:''}
      setTags(tags);
      $("#item_detail_popup").modal("hide");
    }
  };

  useEffect(() => {
    let total = count > 0 ? product.retail_price : 0;

    if (addons.length > 0) {
      addons.forEach((addon) => {
        total = parseFloat(total) + parseFloat(addon.price);
      });
    }

    setTotal((total * count).toFixed(store.round_off));

    //setValue({ quantity: count, product: product, increment: increase });
  }, [count, increase, addons, product]);
  

  const reRenderAttrVal = (i,name,index,ind) => {
// console.log(name,i,index,ind);
// tags = {attribute_value_1:'',attribute_value_2:'',attribute_value_3:''}
    $(".active").removeClass("highlight");

    if(index == 0){
      tags.attribute_value_1 = i
    }else if(index == 1){
      tags.attribute_value_2 = i
    }else{
      tags.attribute_value_3 = i
    }
    setTags(tags);

    $(`li[data-tag='${tags.attribute_value_1}']`).addClass("highlight");
    $(`li[data-tag='${tags.attribute_value_2}']`).addClass("highlight");
    $(`li[data-tag='${tags.attribute_value_3}']`).addClass("highlight");

    let len = 0;
    if(tags.attribute_value_1 != ''){
        len = 1;
    }
     if(tags.attribute_value_2 != ''){
        len = 2;
    }
     if(tags.attribute_value_3 != ''){
        len =3;
    }
    // console.log(Object.entries(product[lang].attributes).length,len);

    if(len == Object.entries(product[lang].attributes).length){
      let item =  product.variants.filter((variant) => {
        if(tags.attribute_value_3 != "" && tags.attribute_value_2 != "" && tags.attribute_value_1 != ""){
          if(variant.attribute_value_1 == tags.attribute_value_1 &&
            variant.attribute_value_2 == tags.attribute_value_2 &&
            variant.attribute_value_3 == tags.attribute_value_3
            ){
            return variant;
          }
        }else if(tags.attribute_value_2 != "" && tags.attribute_value_1 != ""){
          if(variant.attribute_value_1 == tags.attribute_value_1 &&
            variant.attribute_value_2 == tags.attribute_value_2
            ){
            return variant;
          }
        }else if(tags.attribute_value_1 != ""){ 
            if(tags.attribute_value_1 == variant.attribute_value_1){
            // console.log(variant);
              return variant;
            }
          }
      })[0];

      if(item != undefined){
        product.variant_id = item.id; 
        product.retail_price  = item.retail_price ; 
        product.before_discount_price = item.before_discount_price; 
         // console.log(tags);

        if(tags.attribute_value_1 != "" && tags.attribute_value_2 == "" && tags.attribute_value_3 == ""){
          product.selected_variant = product[lang].name+','+tags.attribute_value_1 
        }else if(tags.attribute_value_2 != "" && tags.attribute_value_3 == ""){
           product.selected_variant = product[lang].name+','+ tags.attribute_value_1 +'/'+ tags.attribute_value_2
        }else if(tags.attribute_value_3 != ""){
           product.selected_variant = product[lang].name+','+ tags.attribute_value_1 +'/'+ tags.attribute_value_2 +'/'+tags.attribute_value_3
        }else if(tags.attribute_value_1 != ""){
          product.selected_variant = product[lang].name+','+tags.attribute_value_1 
        }else{
          product.selected_variant = product[lang].name;
        }
 
        // console.log(product.selected_variant);
        dispatch(setPopup(product));
      }
    }
  };

  const render = (i,index) => {
     return (
          <span>
          <b>{ i[0].charAt(0).toUpperCase() + i[0].slice(1)}:</b>
          {i[1].map((ie,ind) => {
          return( 
            <li className="active" keys={`te-${i[0]}`} data-tag={ie}>
                <a
                  className="d-flex flex-column justify-content-between text-center"
                  onClick={() => reRenderAttrVal(ie,i[0],index,ind)}
                >
                  <span>{ie}</span>
                </a>
              </li>
            )
          })}
          </span>
        );
  }

  const renderAttr = () => {
    if(product[lang].attributes != null){
      return Object.entries(product[lang].attributes).map((i, index) => {
          return <ul className="scrollspyArea pt-3">{render(i,index)}</ul>
      });
    }
  };
  
  const renderProduct = () => {
    if (Object.keys(product).length == 0 || product[lang] == undefined) {
      return <div className="modal-body"></div>;
    } else {
      return (
        <div className="modal-body">
          <div className="product_detail">
            <div>
              <img
                loading="lazy"
                src={product.images[0]}
                className="w-100 detail_img"
              />
            </div>
            <div className="d-flex p-3 rounded grey m-3 font-bold justify-content-between">
              <span>{product[lang].name}</span>
              <span>
                {currency} {useCurrencyConverter(parseFloat(product.retail_price))}
              </span>
            </div>
            <div>
              <div className="p-2 pl-4 pr-4">
                {ReactHtmlParser(product[lang].description)}
              </div>
            </div>

            <div className="p-2 pl-3 pr-3">
              {renderAttr()}
            </div>

            
            <div className="extra_selection">
              <div id="accordion2" className="accordion  mt-4 mb-4">
                <div className="card  mb-0">
                  {product[lang].addons.map((addon, index) => {
                    return (
                      <SelectAddOn
                        key={addon.name}
                        addon={addon}
                        counter={index}
                      />
                    );
                  })}
                </div>
              </div>
              {/* <SpecialNotes /> */}
            </div>
          </div>
          {/* Fixed Button Area */}
          <div className="btn_fixed_cart">
            <div className="d-flex align-items-center">
              <div className="p-2">
                {currency} : {useCurrencyConverter(total)}
              </div>
              <div className="p-2">
                <div className="qty_spinner">
                  <form>
                    <button
                      className="value-button minus"
                      id="decrease"
                      onClick={decrement}
                      value="Decrease Value"
                      type="button"
                    >
                      -
                    </button>
                    <input
                      type="number"
                      id="number1"
                      className="number"
                      value={count}
                      disabled="disabled"
                    />
                    <button
                      className="value-button"
                      id="increase"
                      onClick={increment}
                      value="Increase Value"
                      type="button"
                    >
                      +
                    </button>
                  </form>
                </div>
              </div>
              <div className="ml-auto p-2">
                <a href="#" onClick={() => addToCart()}>
                  <span className="iconify" data-icon="bi:cart-plus" />
                </a>
              </div>
            </div>
          </div>
        </div>
      );
    }
  };

  return (
    <div
      className="modal fade item_detail_popup"
      id="item_detail_popup"
      tabIndex={-1}
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog" role="document">
        <div className="modal-content">
          <CloseButton />
          {renderProduct()}
        </div>
      </div>
    </div>
  );
}

export default DetailPopUp;
