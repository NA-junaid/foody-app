import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";
import useTranslation from "../../../services/useTranslation";

function PickUpAddressList() {
  const outlets = useSelector((state) => state.outlets);
  const [selected, setSelected] = useState(0);
  const dispatch = useDispatch();
  const checkoutData = useSelector((state) => state.checkoutData);
  const { pick_from } = useTranslation();

  const onChanged = (val) => {
    setSelected(val);

    checkoutData.pickupOrDeliverySelected = true;
    checkoutData.pickup = true;
    checkoutData.location = pick_from + " " + val.name + ", " + val.street_1,val.city,val.country;
    checkoutData.outletId = val.id;
    checkoutData.area = val.name + ", " + val.street_1,val.city,val.country;
    checkoutData.latitude = val.latitude;
    checkoutData.longitude = val.longitude;

    dispatch(updateCheckoutData(checkoutData));

    $("#pickup_opup").modal("hide");
  }

  const handleClick = (outlet) => {
    onChanged(outlet)
    $('#pickup_opup').modal('hide');
  }  

  return (
    outlets.length > 0 && (
      <div
        className="tab-pane fade"
        id="pills-profile"
        role="tabpanel"
        aria-labelledby="pills-profile-tab"
      >
        {/* Pickup address list*/}
        <ul className="pickup_list">
          {outlets.map((outlet) => {
            if (outlet.pickup == 1) {
              let id = `outlet-${outlet.id}`;
              let idHashed = `#outlet-${outlet.id}`;

              return (
                <li key={outlet.id} onClick={e => handleClick(outlet)} >
                  <h3>
                    <label htmlFor={id}>{outlet.name}</label>
                  </h3>
                  <span>
                    <small>
                      <label htmlFor={id}>
                        {outlet.street_1}, {outlet.outlet_city.city}, {outlet.country}
                      </label>
                    </small>
                  </span>
                  <span className="select_a">
                    <div className="custom-control custom-radio">
                      <input
                        type="radio"
                        id={id}
                        name="customRadio"
                        value={outlet}
                        checked={outlet == selected}
                        onChange={() => onChanged(outlet)}
                        className="custom-control-input"
                        data-dismiss="modal"
                      />
                      <label className="custom-control-label" htmlFor={id} />
                    </div>
                  </span>
                </li>
              );
            }
          })}
        </ul>
      </div>
    )
  );
}

export default PickUpAddressList;
