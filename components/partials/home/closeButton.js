import React, { useState, useEffect } from "react";
function CloseButton() {
    let [tags, setTags] = useState({attribute_value_1:'',attribute_value_2:'',attribute_value_3:''});
    return (
        <div>
            <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
            >
                <span aria-hidden="true">×</span>
            </button>
        </div>
    );
}

export default CloseButton;
