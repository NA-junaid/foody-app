import React from "react";
import useCurrency from "../../../services/useCurrency";
import { useDispatch, useSelector } from "react-redux";
import { addAddon, removeAddon } from "../../../redux/actions/addonAction";
import useLanguage from "../../../services/useLanguage";
import useCurrencyConverter from "../../../services/useCurrencyConverter";

function SelectAddOn({ addon, counter }) {
  const currency = useCurrency();
  const dispatch = useDispatch();
  const store = useSelector((state) => state.store);
  const product = useSelector((state) => state.popup);
  const lang = useLanguage();
  const addons = useSelector((state) => state.addons);

  let id = `collapse21-${counter}`;
  let idHashed = `#collapse21-${counter}`;
  let addonGroup = `group${counter}[]`;

  const toggleItem = (event, item, grounName, group) => {
    item.group = group;
    item.grounName = grounName;
    if (event.target.checked) {
      if (!group) {
        let addon = product[lang].addons.find((a) => a.name == grounName);
        let as = addons.filter((a) => a.grounName == grounName);

        if (addon.max == as.length) {
          event.target.checked = false;
          return;
        }
      }

      dispatch(addAddon(item));
    } else {
      dispatch(removeAddon(item));
    }
  };

  const renderItem = (addon, item) => {
    let selector = `select01-${item.name}-${addon.name}`;

    if (addon.type == "add_on") {
      return (
        <div className="d-flex p-3 justify-content-between" key={item.name}>
          <div>
            <label className="container_check">
              {item.name}
              <input
                type={addon.type == "add_on" ? "checkbox" : "radio"}
                id={selector}
                name={addonGroup}
                className="custom-control-input"
                onChange={(event) =>
                  toggleItem(event, item, addon.name, !(addon.type == "add_on"))
                }
              />
              <span className="checkmark"></span>
            </label>
          </div>
          <label htmlFor={selector}>
            {currency} {useCurrencyConverter(parseFloat(item.price))}
          </label>
        </div>
      );
    } else {
      return (
        <div className="d-flex p-3 justify-content-between" key={item.name}>
          <div className="custom-control custom-radio">
            <input
              type={addon.type == "add_on" ? "checkbox" : "radio"}
              id={selector}
              name={addonGroup}
              className="custom-control-input"
              onChange={(event) =>
                toggleItem(event, item, addon.name, !(addon.type == "add_on"))
              }
            />
            <label className="custom-control-label" htmlFor={selector}>
              {item.name}
            </label>
          </div>
          <label htmlFor={selector}>
            {currency} {useCurrencyConverter(item.price)}
          </label>
        </div>
      );
    }
  };

  let cname = `card-header mb-2 ${counter == 0 ? "" : "collapsed"} `;
  let sybClass=`card-body p-0 collapse ${counter == 0 ? "show" : ""}`;
  let expended = counter == 0
  return (
    <div className="cat1 panel">
      <div className={cname} data-toggle="collapse" href={idHashed} aria-expanded={expended} >
        <a className="card-title">{addon.name}</a>
      </div>
      <div
        id={id}
        className={sybClass}
        data-parent="#accordion2"
        aria-expanded={expended}
      >
        {addon.items.map((item, index) => {
          return renderItem(addon, item);
        })}

        <hr />
      </div>
    </div>
  );
}

export default SelectAddOn;
