import React from "react";
import TabList from "./tabList";
import DeliveryAddressList from "./deliveryAddressList";
import PickUpAddressList from "./pickUpAddressList";

function PopUp() {
    
    return (
        <div className="modal fade delivery_pickup_opup" id="pickup_opup" tabIndex={-1} role="dialog" aria-hidden="true" >
            <div className="modal-dialog modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-body">
                        <TabList />
                        <div className="tab-content" id="pills-tabContent">
                            <DeliveryAddressList />
                            <PickUpAddressList />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PopUp;
