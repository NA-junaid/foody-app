import React, { useEffect, useState } from "react";
import Delivery from "./delivery";
import PickUp from "./pickUp";
import Change from "./change";
import SearchArea from "./searchArea";
import { useSelector, useDispatch } from "react-redux";
import PopUp from "./popUp";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";

function LocationInfo() {
  const checkoutData = useSelector((state) => state.checkoutData);
  const { location, pickup, pickupOrDeliverySelected } = checkoutData;

  const renderInfo = () => {
    if (checkoutData.pickupOrDeliverySelected) {
      return (
        <div className="col-md-12 selected_area">
          <div className="d-flex p-1 font-bold justify-content-between">
            <span>
              <span
                className="iconify"
                data-inline="false"
                data-icon="carbon:location"
              />
              {checkoutData.location}
            </span>
            <Change />
          </div>
        </div>
      );
    } else {
      return <div className="col-md-12"></div>;
    }
  };

  useEffect(() => {
  }, [location, pickup, pickupOrDeliverySelected]);

  return renderInfo();
}

function SearchFields() {
  const areas = useSelector((state) => state.areas);
  const outlets = useSelector((state) => state.outlets);
  const checkoutData = useSelector((state) => state.checkoutData);
  const dispatch = useDispatch();
  const [init, setInit] = useState(false);

  useEffect(() => {
    if (!init) {

      // if (areas.length == 0) {
      //   checkoutData.pickupOrDeliverySelected = true;
      // }

      // dispatch(updateCheckoutData(checkoutData));
      // setInit(true);
    }
  }, [false]);

  return (
    <div>
      <div className="search_area">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="d-flex d-flex2 justify-content-between">
                {areas.length > 0 ? <Delivery /> : null}
                {outlets.length > 0 ? <PickUp /> : null}
              </div>
            </div>
            <LocationInfo />
          </div>
          <SearchArea />
        </div>
      </div>
      <PopUp />
    </div>
  );
}

export default SearchFields;
