import React from "react";
import ItemImage from "./itemImage";
import ItemDetail from "./itemDetail";
import ShowCart from "./showCart";
import ShowQuantity from "./showQuantity";
import useLanguage from "../../../services/useLanguage";
import useCurrency from "../../../services/useCurrency";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setPopup } from "../../../redux/actions/popupAction";
import { discardAddons } from "../../../redux/actions/addonAction";
import useCurrencyConverter from "../../../services/useCurrencyConverter";

function Products({ product }) {
  const lang = useLanguage();
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();
  const currency = useCurrency();
  const store = useSelector((state) => state.store);

  const toggle = () => setShow((o) => !o);

  const setPopupData = () => {
    dispatch(setPopup(product));
    dispatch(discardAddons());
  };

  const renderControl = () => {
    if (product[lang].addons.length > 0 || product.total_variants > 1) {
      return (
        <a
          className="product_detail_ff"
          data-toggle="modal"
          data-target="#item_detail_popup"
          onClick={setPopupData}
        >
          <i className="icon-arrow-right"></i>
        </a>
      );
    } else {
      return (
        <a title="Cart" onClick={toggle}>
          <img loading="lazy" src="/assets/img/cart_icon.svg" />
        </a>
      );
    }
  }

  const renderListHandler = () => {
    if (product[lang].addons.length > 0 || product.total_variants > 1) {
      setPopupData();
      $('#item_detail_popup').modal('show');
    }
  }

  return (
    <div className="row align-items-center" onClick={e => renderListHandler() } >
      <div className="col-md-4">
        <span className="item_img">
          <img loading="lazy" src={product["image"]} className="w-100" />
        </span>
      </div>
      <div className="col-md-8">
        <div className="item_detail">
          <h3>{product[lang]["name"]} </h3>
          <p className="price">
            <span>
              {product.before_discount_price != null
                ? `${currency} ${useCurrencyConverter(parseFloat(
                    product.before_discount_price
                  ))}`
                : ``}
            </span>
            <small>
              {currency}{" "}
              {useCurrencyConverter(parseFloat(product.retail_price))}
            </small>
          </p>
        </div>
        <span className="show_cart">{renderControl()}</span>
        {show ? <ShowQuantity product={product} /> : ""}
      </div>
    </div>
  );
}

export default Products;
