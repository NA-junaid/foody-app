import React from "react";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";
function Delivery() {
  const checkoutData = useSelector((state) => state.checkoutData);

  const { Delivery } = useTranslation();

  const deliveryPopUp = () => {
    setTimeout(function () {
      $("#pills-home-tab").trigger("click");
    }, 100);
  };

  return (
    <div className="custom-control custom-radio">
      <input
        type="radio"
        id="customRadio1"
        name="pickup"
        className="custom-control-input"
        onClick={deliveryPopUp}
        checked={checkoutData.pickupOrDeliverySelected && !checkoutData.pickup}
        onChange={() => console.log("Delivery")}
      />
      <label
        className="custom-control-label"
        data-toggle="modal"
        data-target="#pickup_opup"
        htmlFor="customRadio1"
      >
        {Delivery}
      </label>
    </div>
  );
}

export default Delivery;
