import React from "react";
import Link from "next/link";
import useCurrency from "../../../services/useCurrency";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";
import Router from "next/router";
import useCurrencyConverter from "../../../services/useCurrencyConverter";

function CartDetailArea() {
  const [total, setTotal] = useState(0);
  const [items, setItems] = useState(0);
  const currency = useCurrency();
  const cart = useSelector((state) => state.cart);
  const areas = useSelector((state) => state.areas);
  const outlets = useSelector((state) => state.outlets);
  const settings = useSelector((state) => state.webSetting);
  let styleClass = `col-md-8`; //${settings.whatsapp_number == null ? 12 : 8}
  const checkoutData = useSelector((state) => state.checkoutData);
  const [link, setLink] = useState(0);

  const handleChange = () => {
    if (cart.items.length == 0) {
      //toastr.error('Your cart is empty');
      return false;
    }

    if (!checkoutData.pickupOrDeliverySelected) {
      console.log(areas);
      if (areas.length > 0) {
        $("#pickup_opup").modal("show");
        $("#pills-home-tab").trigger("click");
      } else {
        setTimeout(function () {
          $("#pickup_opup").modal("show");
          $("#pills-profile-tab").trigger("click");
        }, 100);
      }
    } else {
      Router.push("/cart");
    }
  };

  useEffect(() => {
    setTimeout(() => {
      setItems(cart.items.length);
      setTotal(cart.total);
    }, 500);
  }, [total, items, cart, checkoutData.pickupOrDeliverySelected]);

  return (
    <div className={styleClass}>
      <span className="qty">{cart.items.length}</span> {currency} :{" "}
      {useCurrencyConverter(cart.total)}
      <span className="cart">
        <a href="#" onClick={(event) => handleChange(event)}>
          <span className="iconify" data-icon="mdi:cart-arrow-right" />
        </a>
      </span>
    </div>
  );
}

export default CartDetailArea;
