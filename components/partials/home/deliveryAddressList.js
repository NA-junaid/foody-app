import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { updateCheckoutData } from "../../../redux/actions/checkoutDataAction";
import useTranslation from "../../../services/useTranslation";

function DeliveryAddressList() {
  const areas = useSelector((state) => state.areas);
  const checkoutData = useSelector((state) => state.checkoutData);
  const [selected, setSelected] = useState(checkoutData.name);
  const dispatch = useDispatch();
  const { deliver_to } = useTranslation();

  const getAddressByInput = async (keyword) => {
    $.ajax({
      url:
        "https://maps.googleapis.com/maps/api/geocode/json?address=" +
        keyword +
        "&sensor=false&key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc",
      success: function (data) {
        checkoutData.latitude = data.results[0].geometry.location.lat;
        checkoutData.longitude = data.results[0].geometry.location.lng;

        dispatch(updateCheckoutData(checkoutData));
      },
    });
  };  

  const handleChange = (val) => {
    setSelected(val.name);

    checkoutData.pickupOrDeliverySelected = true;
    checkoutData.pickup = false;
    checkoutData.location = deliver_to + " " + val.name;
    checkoutData.area = val.name;
    checkoutData.outletId = val.outlet_id;
  
    dispatch(updateCheckoutData(checkoutData));

    getAddressByInput(val.name)
  }

  const handleClick = (area) => {
    handleChange(area)
    $('#pickup_opup').modal('hide');
  }

  return (
    areas.length > 0 && (
      <div
        className="tab-pane fade"
        id="pills-home"
        role="tabpanel"
        aria-labelledby="pills-home-tab"
      >
        {/* Delivery address list*/}
        <ul className="pickup_list">
          {areas.map((area) => {
            let id = `area-${area.id}`;
            return (
              <li key={area.id} htmlFor={id} onClick={e => handleClick(area)}>
                <h3>
                  <label htmlFor={id}>{area.name}</label>
                </h3>
                <span className="select_a">
                  <div className="custom-control custom-radio">
                    <input
                      type="radio"
                      id={id}
                      name="customRadio1"
                      value={area.name}
                      checked={area.name == selected}
                      onChange={() => handleChange(area)}
                      className="custom-control-input"
                      data-dismiss="modal"
                    />
                    <label className="custom-control-label" htmlFor={id} />
                  </div>
                </span>
              </li>
            );
          })}
        </ul>
      </div>
    )
  );
}

export default DeliveryAddressList;
