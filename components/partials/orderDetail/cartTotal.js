import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";
function CartTotal({order}) {
  const { sub_total , Delivery , total } = useTranslation();
  const store = useSelector((state) => state.store);
  
  return (
    <div className="d-flex p-3 mt-2 mb-5 justify-content-between cart_total">
      <span>
        <p>{sub_total}</p>
        <p>{Delivery}</p>
        <p>
          <b>{total}</b>
        </p>
      </span>
      <span className="text-right">
        <p>{store.currency} : {order.subtotal}</p>
        <p>{store.currency} : {order.shipping_rate}</p>
        <p>
          <b>{store.currency} : {order.total}</b>
        </p>
      </span>
    </div>
  );
}
export default CartTotal;