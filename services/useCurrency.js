import { useSelector } from "react-redux";

function useCurrency() {
  const currencies = useSelector((state) => state.currencies);
  const filtered = currencies.filter((item) => item.isDefault);
  const currentCurrency = filtered[0];
  
  return currentCurrency != undefined ? currentCurrency.name : '';
}

export default useCurrency;
