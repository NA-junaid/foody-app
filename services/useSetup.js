import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addPage } from "../redux/actions/pageAction";
import { addTranslation } from "../redux/actions/translationAction";
import { addCategory } from "../redux/actions/categoryAction";
import { addLanguage } from "../redux/actions/languageAction";
import { setAllCurrencies } from "../redux/actions/setAllCurrencies";
import { setStore } from "../redux/actions/storeAction";
import { addPaymentMethod } from "../redux/actions/paymentMethodActions";
import { addShippingMethod } from "../redux/actions/shippingMethodActions";
import { setWebSetting } from "../redux/actions/webSettingAction";
import { setSocialLinks } from "../redux/actions/socialLinksAction";
import { addCurrency } from "../redux/actions/currencyAction";
import { addOutlet } from "../redux/actions/outletAction";
import { addPlugin } from "../redux/actions/pluginAction";
import { addZone } from "../redux/actions/zoneAction";
import { addArea } from "../redux/actions/areaAction";
import { addProduct } from "../redux/actions/productAction";
import { saveState, removeState } from "../services/storage";
import store from "../redux/index";

const reduxStore = store;

let initialState = {
  languages: [],
  translations: [],
  pages: [],
  categories: [],
  store: {},
  payment_methods: [],
  shipping_methods: [],
  web_setting: {},
  social_links: [],
};

function useSetup(props) {
  const dispatch = useDispatch();
  const [setup, setSetup] = useState({});

  const initialize = async () => {
    const {
      pages,
      translations,
      categories,
      languages,
      store,
      payment_methods,
      shipping_methods,
      web_setting,
      web_social_link,
      all_currencies,
      currencies,
      outlets,
      store_plugin,
      zones_list,
      areas_list,
      products,
    } = props;

    removeState("state");

    if (pages.length > 0) {
      pages.map((page) => {
        dispatch(addPage(page));
      });
    }

    if (translations != null) {
      dispatch(addTranslation(translations));
    }

    if (categories.length > 0) {
      categories.map((category) => {
        dispatch(addCategory(category));
      });
    }

    if (languages.length > 0) {
      languages.map((language, index) => {
        language.isDefault = index == 0 ? true : false;
        dispatch(addLanguage(language));
      });
    }

    if (payment_methods.length > 0) {
      payment_methods.map((payment_method) => {
        dispatch(addPaymentMethod(payment_method));
      });
    }

    if (shipping_methods.length > 0) {
      shipping_methods.map((shipping_method) => {
        dispatch(addShippingMethod(shipping_method));
      });
    }

    if (web_setting != null) {
      dispatch(setWebSetting(web_setting));
    }

    if (web_social_link != null) {
      web_social_link.map((social_link) => {
        dispatch(setSocialLinks(social_link));
      });
    }

    if (currencies.length > 0) {
      currencies.map((currency) => {
        dispatch(
          addCurrency({ name: currency, isDefault: currency  == web_setting.default_currency})
        );
      });
    }
    if (store != null) {
      all_currencies.find((currency) => {
          if(currency.currency_symbol == web_setting.default_currency){
            store.round_off = currency.round_off;
          }
      });
      dispatch(setAllCurrencies(all_currencies));
      dispatch(setStore(store));
    }

    if (outlets.length > 0) {
      outlets.map((outlet) => {
        dispatch(addOutlet(outlet));
      });
    }

    if (store_plugin.length > 0) {
      store_plugin.map((plugin) => {
        dispatch(addPlugin(plugin));
      });
    }

    if (zones_list.length > 0) {
      zones_list.map((zone) => {
        dispatch(addZone(zone));
      });
    }

    if (areas_list.length > 0) {
      areas_list.map((area) => {
        dispatch(addArea(area));
      });
    }

    if (products.length > 0) {
      products.map((product) => {
        dispatch(addProduct(product));
      });
    }
    saveState("state", reduxStore.getState());
  };

  useEffect(() => {
    initialize();
  });
  return [setup, setSetup];
}

export default useSetup;
