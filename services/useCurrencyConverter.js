import { useSelector } from "react-redux";
import useCurrency from "../services/useCurrency";
import store from "../redux";

function useCurrencyConverter(price) {
  const state = store.getState();
  const { all_currencies, websetting, currencies } = state;

  const filtered = currencies.filter((item) => item.isDefault);
  const currentCurrency = filtered[0];

  let selected_currency = [];

  if (all_currencies[0] != undefined) {
    const currency = currentCurrency.name;
    selected_currency = all_currencies.filter(
      (item) => item.currency_symbol == currency
    );

    if (
      websetting != undefined &&
      selected_currency[0].currency_symbol != websetting.default_currency
    ) {
      return (parseFloat(price) * selected_currency[0].rate_in_usd).toFixed(
        selected_currency[0].round_off
      );
    } else {
      return parseFloat(price).toFixed(
        selected_currency[0] != undefined ? selected_currency[0].round_off : 0
      );
    }
  } else {
    return parseFloat(price).toFixed(
      selected_currency[0] != undefined ? selected_currency[0].round_off : 0
    );
  }
}

export default useCurrencyConverter;
