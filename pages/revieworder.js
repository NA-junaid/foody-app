import SubHeader from "../components/common/subHeader";
import SubTitles from "../components/common/subTitles";
import CartItemList from "../components/partials/reviewOrder/cartItemList";
import GeneralInfo from "../components/partials/reviewOrder/generalInfo";
import DeliveryInfo from "../components/partials/reviewOrder/deliveryInfo";
import PaymentInfo from "../components/partials/reviewOrder/paymentInfo";
import SpecialNotes from "../components/partials/reviewOrder/specialNotes";
import CartTotal from "../components/partials/reviewOrder/cartTotal";
import useTranslation from "../services/useTranslation";
import useCheckoutDataHolder from "../services/useCheckoutDataHolder";
import { useSelector, useDispatch } from "react-redux";
import React, { useState, useEffect } from "react";
import urlService from "../services/urlService";
import networkService from "../services/networkService";
import store from "../redux/index";

export default function ReviewOrder() {
  const {
    Payment_Info,
    shipping_method,
    specialnotes,
    min_order_amount_of,
    Place_Order,
  } = useTranslation();

  const {
    Review_Order,
    Order_Items,
    Delivery_Info,
    Avenue,
    apartment_or_office,
    street_number,
    area_name,
    city,
    country,
  } = useTranslation();
  const checkoutData = useSelector((state) => state.checkoutData);
  const store = useSelector((state) => state.store);
  const {
    success,
    name,
    email,
    phone,
    paymentMethod,
    shippingMethod,
    outlet,
    avanue,
    housenumber,
    streetnumber,
    areaname,
  } = checkoutData;
  const webSetting = useSelector((state) => state.webSetting);
  const outlets = useSelector((state) => state.outlets);
  const cart = useSelector((state) => state.cart);
  const reduxStore = store;
  var message;

  const paymentMethods = useSelector((state) => state.paymentMethods);
  const shippingMethods = useSelector((state) => state.shippingMethods);
  let [payment_Method, setPaymentMethod] = useState();
  let [shipping_Method, setShippingMethod] = useState();
  const [update, setUpdate] = useState();
  const initialize = (checkoutData) => {
    console.log(checkoutData)
    console.log(webSetting)
    console.log(shippingMethods)
    setUpdate(false);
    let { paymentMethod , shippingMethod } = checkoutData;

    console.log(paymentMethod , shippingMethod)

    let filteredPm = paymentMethods.find(pm => pm.id == paymentMethod);
    console.log(filteredPm);
    setPaymentMethod(filteredPm);
    let filteredSm = shippingMethods.find(sm => sm.id == shippingMethod)
    setShippingMethod(filteredSm);
    console.log(payment_Method,shipping_Method);
    setUpdate(true);
  };
  useEffect(() => {
    if (!update) {
      initialize(checkoutData);
    }
  });
  const placeOrder = async () => {
    let new_cart = [];
    let new_addon = [];
    cart.items.map(
      (item) => (
        (new_addon = []),
        item.addons.map((add) =>
          new_addon.push({
            name: add.name,
            price: add.price,
          })
        ),
        new_cart.push({
          addon: new_addon,
          product_id: item.product_id,
          qty: item.quantity,
          price: item.retail_price,
          total: item.total,
          variant_id: item.variant_id,
        })
      )
    );

    let outlet_id = outlets[0] != null ? outlets[0].id : "";
    let shipping_rate =
      checkoutData.shippingMethod != null ? checkoutData.shippingRate : 0;
    let address = "";
    if (checkoutData.pickup == true) {
      outlet_id =
        checkoutData.outletId != undefined ? checkoutData.outletId : "";
      address = checkoutData.area;
    } else {
      if (checkoutData.area != null) {
        outlet_id = checkoutData.area.outlet_id;
      }
      shipping_rate =
        checkoutData.shippingMethod != null ? checkoutData.shippingRate : "";
      let city = checkoutData.city != undefined ? ", " + checkoutData.city : "";
      let country =
        checkoutData.country != undefined ? ", " + checkoutData.country : "";
      address =
        checkoutData.areaInfo.avanue +
        ", " +
        checkoutData.areaInfo.housenumber +
        ", " +
        checkoutData.areaInfo.streetnumber +
        ", " +
        checkoutData.areaInfo.block +
        ", " +
        checkoutData.area +
        city +
        country;
    }
    const checkout_obj = {
      address_id: 0,
      shipping: checkoutData.shippingMethod != null ? shipping_Method.id : "",
      payment_method:
        checkoutData.paymentMethod != null ? payment_Method.id : "",
      country: "",
      company: "",
      address: address,
      city: "",
      county: "",
      avenue: checkoutData.avanue,
      house_detail: checkoutData.housenumber,
      street: checkoutData.streetnumber,
      block: checkoutData.block,
      area: checkoutData.areaname,

      name: checkoutData.name,
      email: checkoutData.email,
      phone: checkoutData.phone,
      user_firstName: checkoutData.name,
      user_lastName: "",
      user_mobile: checkoutData.phone,
      user_email: checkoutData.email,
      delivery_date: "",
      delivery_time:
        checkoutData.shipping_Method != null
          ? shipping_Method.delivery_time
          : "",
      re_subscription_days: "",
      applyReward: "",
      reward_value: "",
      guest_login: "",
      notes: checkoutData.direction,
      outlet_id: outlet_id,
      shipping_rate:
        checkoutData.shippingRate > 0
          ? checkoutData.shippingRate
          : shipping_Method != null
          ? checkoutData.shippingRate
          : "",
      is_pickup: checkoutData.pickup,
      discount_voucher_code: checkoutData.discount_voucher_code,
      applyReward:checkoutData.loyalty_reward_amount > 0 ? 'on' : '',
      reward_value: checkoutData.loyalty_reward_amount > 0 ? checkoutData.loyalty_reward_amount : 0,
      order_prepare_time: checkoutData.deliveryTime != "" ? checkoutData.deliveryTime :  webSetting.order_prepare_time,
      cart: JSON.stringify(new_cart),
    };

    const response = await networkService.post(
      urlService.postOrderSave,
      checkout_obj
    );

    if (response != null && response.IsValid) {
      if (response.Payload.payment_url != null) {
        const win = window.open(response.Payload.payment_url, "_blank");
        if (win != null) {
          win.focus();
        }
      }
      if (response.Payload.success) {
        const win = window.location.replace(
          "thanks?order_number=" + response.Payload.order_number
        );
      }
    } else {
      if (response != null && response.Errors == null) {
        toastr.success(success);
      } else {
        if (response != null) {
          toastr.error(response.Errors);
        }
      }
    }
  };
  const renderPaymentMethod = () => {
    if (paymentMethod != null) {
      return (
        <div>
          <h2 className="text-uppercase pt-4 pb-3"> {Payment_Info} </h2>
          <p>
            <span
              className="iconify checked_icon"
              data-icon="ant-design:check-circle-filled"
            />
            {payment_Method != null ? payment_Method.name : ""}
          </p>
          <hr />
        </div>
      );
    }
  };

  const renderShippingInfo = () => {
    if (!checkoutData.pickup && shipping_Method != null) {
      return (
        <div>
          <h2 className="text-uppercase pt-4 pb-3"> {shipping_method} </h2>
          <p>
            <span
              className="iconify checked_icon "
              data-icon="ant-design:check-circle-filled"
            />
            <p className=" d-flex" style={{ justifyContent: "space-between" }}>
              <p class="card-link">{shipping_Method.name}</p>
              <p class="card-link">
                {webSetting.default_currency}:{" "}
                {parseFloat(checkoutData.shippingRate).toFixed(store.round_off)}
              </p>
            </p>
          </p>
          <hr />
        </div>
      );
    }
  };

  const renderPickupOrDelivery = () => {
    if (checkoutData.pickup) {
      return (
        <div>
          <h3 className="mt-4 mb-2">Pickup</h3>
          <div class="mt-3">
            <p>{checkoutData.area}</p>
            <hr />
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <h3 className="mt-4 mb-2">{Delivery_Info}</h3>
          {checkoutData.areaInfo.avanue != undefined &&
          checkoutData.areaInfo.avanue != "" ? (
            <div className="mt-3">
              <h6>{Avenue}</h6>
              <p>{checkoutData.areaInfo.avanue}</p>
              <hr />
            </div>
          ) : (
            ""
          )}
          <div className="mt-3">
            <h6>{apartment_or_office}</h6>
            <p>{checkoutData.areaInfo.housenumber}</p>
            <hr />
          </div>
          <div className="mt-3">
            <h6>{street_number}</h6>
            <p>{checkoutData.areaInfo.streetnumber}</p>
            <hr />
          </div>
          <div className="mt-3">
            <h6>{area_name}</h6>
            <p>{checkoutData.area}</p>
            <hr />
          </div>
          {checkoutData.city != undefined ? (
            <div className="mt-3">
              <h6>{city}</h6>
              <p>{checkoutData.city}</p>
              <hr />
            </div>
          ) : (
            ""
          )}
          {checkoutData.country != undefined ? (
            <div className="mt-3">
              <h6>{country}</h6>
              <p>{checkoutData.country}</p>
              <hr />
            </div>
          ) : (
            ""
          )}
        </div>
      );
    }
  };
  const renderMinOrder = () => {
    if (parseFloat(webSetting.min_order_value) > parseFloat(cart.total)) {
      return (
        <button
          style={{ width: "100%", height: "60px", "z-index": "1" }}
          className="btn btn-danger nextBtn btn-lg pull-right mt-4 mb-2"
        >
          {min_order_amount_of} {webSetting.min_order_value}{" "}
          {webSetting.default_currency}
        </button>
      );
    }
  };
  const renderPLaceOrder = () => {
    if (parseFloat(webSetting.min_order_value) < parseFloat(cart.total)) {
      return (
        <div className="row setup-content">
          <div className="col-md-12">
            <button
              className="btn btn-primary nextBtn btn-lg reviewOrderBtn pull-right"
              type="button"
              onClick={() => placeOrder()}
              //disabled={isSubmitting}
            >
              {Place_Order}
            </button>
          </div>
        </div>
      );
    }
  };
  return (
    <div>
      <SubHeader title={Review_Order} />

      <div className="white_wrapper cart">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <GeneralInfo uName={name} uEmail={email} uPhone={phone} />
              <hr />
              {renderPickupOrDelivery()}
              {renderPaymentMethod()}
              {renderShippingInfo()}

              {checkoutData.direction != undefined &&
              checkoutData.direction != "" ? (
                <div>
                  <div>
                    <div className="mt-3">
                      <h6>{specialnotes}</h6>
                    </div>
                    <p className="mt-3">{checkoutData.direction}</p>
                  </div>
                  <hr />
                </div>
              ) : (
                ""
              )}
              <SubTitles title={Order_Items} />
              <hr />
              <CartItemList />
            </div>
          </div>
        </div>
      </div>
      <CartTotal />

      {renderPLaceOrder()}
      {renderMinOrder()}
    </div>
  );
}
