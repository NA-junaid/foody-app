import SubHeader from "../components/common/subHeader";
import WhiteWrapper from "../components/partials/editAccount/whiteWrapper";
import useTranslation from "../services/useTranslation";

export default function EditAccount() {
  const { edit_account } = useTranslation();
  return (
    <div>
      <SubHeader title={edit_account} />
      <WhiteWrapper />
    </div>
  );
}