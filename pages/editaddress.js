import React from "react";
import SubHeader from "../components/common/subHeader";
import WhiteWrapper from "../components/partials/editAddress/whiteWrapper";
import useTranslation from "../services/useTranslation";

export default function EditAddress() {
  const { edit_add } = useTranslation();
  return (
    <div>
      <SubHeader title={edit_add}/>
      <WhiteWrapper />
    </div>
  );
}