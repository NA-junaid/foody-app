import SubHeader from "../components/common/subHeader";
import WhiteWrapper from "../components/partials/orderHistory/whiteWrapper";
import useTranslation from "../services/useTranslation";
import urlService from "../services/urlService";
import networkService from "../services/networkService";
import { useEffect, useState } from "react";

export default function OrderHistory() {
  let [orders, setOrders] = useState();
  const { order_history } = useTranslation();
  let [init, setInit] = useState(false);

  const initialize = async () => {
    const response = await networkService.get(urlService.getOrderIndex);
    if (response.IsValid) {
      setOrders(response.Payload.data);
      setInit(true);
    }
    
  }

  useEffect(() => {
    if(!init){
      initialize();
    }
  },[orders]);

  return (
      <div>
  
          <SubHeader title={order_history} />
          <WhiteWrapper orders={orders}/>
      </div>
  );
}