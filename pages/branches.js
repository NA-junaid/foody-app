import React from "react";
import SubHeader from "../components/common/subHeader";
import WhiteWrapper from "../components/partials/branches/whiteWrapper";
import useTranslation from "../services/useTranslation";
import networkService from "../services/networkService";
import urlService from "../services/urlService";
import { useSelector, useDispatch } from "react-redux";
import { withScriptjs, withGoogleMap, GoogleMap, Marker} from "react-google-maps";


function Maps(){
  const outlets = useSelector((state) => state.outlets);
  let firstoutlet = {...outlets[0]};
  let firstoutletlatitude = firstoutlet.latitude;
  let firstoutletlongitude = firstoutlet.longitude;
  let latlng = {
    lat: parseFloat(firstoutletlatitude),
    lng: parseFloat(firstoutletlongitude)
  }


  const MapWithAMarker = withScriptjs(withGoogleMap(props =>
    <GoogleMap
      defaultZoom={15}
      defaultCenter={{...props.latlng}}
    >
  {/*{props.outlets.map((outlet) => {
      <Marker
        position = {{lat: parseFloat(outlet.latitude), lng: parseFloat(outlet.longitude)}}
      />
    })}*/}
      <Marker
        position = {{...props.latlng}}
      />
    </GoogleMap>
  ));

     const { Branches } = useTranslation();
    return(
      <span>
      <SubHeader title={Branches} />
        <div className="map ml-3 mr-3 mt-3 mb-3" style={{ width: '94%',height: '200px'}}>
          <span>
            <MapWithAMarker 
               latlng={latlng}
               outlets={outlets}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc&v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={<div style={{ height: `200px` }} />}
              mapElement={<div style={{ height: `200px` }} />}
            />
          </span>
        </div>
        </span>
    );
}
export default Maps;




