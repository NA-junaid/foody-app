import React from "react";
import SubHeader from "../components/common/subHeader";
import ProductGridItem from "../components/partials/home/productGridItem";
import PopUp from "../components/partials/home/popUp";
import DetailPopUp from "../components/partials/home/detailPopUp";
import FixedButtonArea from "../components/fixedButtonArea";
import useLanguage from "../services/useLanguage";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";

function Home() {
  const router = useRouter();
  const { category_id } = router.query;
  const categories = useSelector((state) => state.categories);
  const products = useSelector((state) => state.products);

  const category = categories.find((c) => c.id == category_id);
  const filteredProducts = products.filter((p) => p.category_id == category_id);
  const lang = useLanguage();

  return (
    <div>
      <SubHeader title={category != undefined ? category[lang] : ''} type={1} />
      <div className="home2_products_area">
        <div className="products_row">
          <div className="product_scroller_3">
            <ul className="inner_pro">
              {filteredProducts.map((product) => {
                return <ProductGridItem data={product} />;
              })}
            </ul>
          </div>
        </div>
      </div>
      <PopUp />
      <DetailPopUp />
      <FixedButtonArea />
    </div>
  );
}

export default Home;
