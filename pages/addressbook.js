import React, { useEffect } from "react";
import SubHeader from "../components/common/subHeader";
import WhiteWrapper from "../components/partials/addressBook/whiteWrapper";
import useTranslation from "../services/useTranslation";
import networkService from "../services/networkService";
import urlService from "../services/urlService";
export default function AddressBook(props) {
  
  
  const { address_book } = useTranslation();
  return (
    <div>
      <SubHeader title={address_book} />
      <WhiteWrapper />
    </div>
  );
}

