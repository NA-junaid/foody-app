import ThanksImage from "../components/partials/thanks/thanksImage";
import ConfirmMessage from "../components/partials/thanks/confirmMessage";
import OrderCode from "../components/partials/thanks/orderCode";
import NeedHelp from "../components/partials/thanks/needHelp";
import Router from "next/router";
import useTranslation from "../services/useTranslation";
import { useEffect } from "react";

export default function Thanks() {
  const { Start_Over } = useTranslation();

  useEffect(() => {
    setTimeout(() => {
      $(".proceedBtn").show();
    }, 100);
  });

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12 mt-5 text-center">
          <ThanksImage />
          <ConfirmMessage />
          <OrderCode />
          <div className="row setup-content">
            <div className="col-md-12">
              <button
                className="btn btn-primary nextBtn proceedBtn btn-lg"
                type="button"
                onClick={() => Router.push("/")}
                style={{ position: "initial" }}
              >
                {Start_Over}
              </button>
            </div>
          </div>
          <hr className="mt-5 mb-5" />
          <NeedHelp />
        </div>
      </div>
    </div>
  );
}
