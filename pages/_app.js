import React, { useEffect } from "react";

import PageHead from "../components/pagehead";
import Footer from "../components/footer";
import Sub_Footer from "../components/subfooter";
import MySideNav from "../components/sidenav";
import NewsLetter from "../components/newsletter";
import Script from "../components/script";
import Header from "../components/common/header";
import { Provider } from "react-redux";
import store from "../redux/index";
import Router from "next/router";
import { setBack } from "../redux/actions/routeAction";

export default function MyApp({ Component, pageProps }) {
  const Layout = Component.Layout ? Component.Layout : React.Fragment;

  useEffect(() => {
    if (typeof window === "undefined") {
      global.window = {};
    }
  }, []);

  const doMagic = () => {
    let currentRoute = Router.router.route.substr(1);
    const items = [
      "loginregister",
      "branches",
      "orderstatus",
      "contactus",
      "myaccount",
      "",
    ];
    if (items.includes(currentRoute)) {
      store.dispatch(setBack(false));
    } else {
      store.dispatch(setBack(true));
    }
    if (process.browser) {
      $('.contentMenuWrapper').animate({ scrollTop: 0 }, 'fast');
    }
  };

  useEffect(() => {
    Router.events.on("routeChangeComplete", doMagic); // add listener

    return () => {
      Router.events.off("routeChangeComplete", doMagic); // remove listener
    };
  }, []);

  return (
    <Provider store={store}>
      <Layout>
        <PageHead />
        <main role="main" className="main_wrapper" id="main-wrapper">
          <div className="contentMenuWrapper">
            <div className="contentFood">
              <div className="content">
                <Header />
                <MySideNav />
                <Component {...pageProps} />
                <NewsLetter />
                <Footer />
                <Sub_Footer />
              </div>
            </div>
          </div>
        </main>
        <Script />
      </Layout>
    </Provider>
  );
}
