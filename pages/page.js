import useLanguage from "../services/useLanguage";
import networkService from "../services/networkService";
import useTranslation from "../services/useTranslation";
import urlService from "../services/urlService";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ReactHtmlParser from "react-html-parser";
import SubHeader from "../components/common/subHeader";

export default function Page() {
  const lang = useLanguage();
  const router = useRouter();
  const [page, setPage] = useState();
  const { slug } = router.query;
  const {contact_us} = useTranslation();

  const getPage = async (slug) => {
    let url = urlService.getPage + "/" + slug;
    const response = await networkService.get(url);

    if (response != undefined && response.IsValid) {
      setPage(response.Payload);
    }
  };

  useEffect(() => {
    getPage(slug);
  }, [slug]);

  const renderPage = () => {
    if (page != undefined) {
      return (
        <div>
          <SubHeader title={page[lang].name} />
          <div className="white_wrapper cart">
            <div className="container">
              <div className="row">
                <div className="col-md-12 mt-3">
                  <p>{page[lang].name}</p>
                  {ReactHtmlParser(page[lang].description)}
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      <div></div>;
    }
  };

  return <div>{renderPage()}</div>;
}
