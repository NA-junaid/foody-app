import React from "react";
import SubHeader from "../components/common/subHeader";
import WhiteWrapper from "../components/partials/cart/whiteWrapper";
import useTranslation from "../services/useTranslation";


export default function Cart() {
    const { my_cart } = useTranslation();
    return (
        <div>
            <SubHeader title={my_cart} />
            <WhiteWrapper />
        </div>
    );
}