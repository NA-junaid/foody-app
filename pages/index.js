import Slider from "../components/partials/home/slider";
import VendorDetail from "../components/partials/home/vendorDetail";
import SearchFields from "../components/partials/home/searchFields";

import DetailPopUp from "../components/partials/home/detailPopUp";
import FixedButtonArea from "../components/fixedButtonArea";
import networkService from "../services/networkService";
import urlService from "../services/urlService";
import useSetup from "../services/useSetup";
import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Home3Products from "../components/partials/home/home3Products";
import Home4Products from "../components/partials/home/home4Products";
import Home5Products from "../components/partials/home/home5Products";
import SearchModel from "../components/common/searchModel";
import { updateCheckoutData } from "../redux/actions/checkoutDataAction";

export default function Home(props) {
  const [initialize, setInitialize] = useState(false);
  const [setup, setSetup] = useSetup(props);
  const { banners, products } = props;
  const [type, setType] = useState(0);
  const checkoutData = useSelector((state) => state.checkoutData);
  const dispatch = useDispatch();

  const init = async () => {
    const response = await networkService.get('websetting');
    if (response.IsValid) {
      $("#main-wrapper").css({ background: `url(${response.Payload.url})` });
      setType(response.Payload.sub_theme);
    }
  };

  useEffect(() => {
    setInitialize(true);

    checkoutData.deliveryTime = props.web_setting != undefined ? props.web_setting.order_prepare_time : '' ;
    dispatch(updateCheckoutData(checkoutData));

    setTimeout(() => {
      init();
    }, 1000);
  }, [false]);

  const selectLayout = (type) => {
    if (type == 1) {
      return <Home3Products />;
    } else if (type == 2) {
      return <Home4Products />;
    } else if (type == 3) {
      return <Home5Products />;
    }
  };

  return (
    <div>
      <Slider banners={banners} />
      <VendorDetail />
      <hr />
      <SearchFields />

      {selectLayout(type)}

      <SearchModel products={products} />
      <DetailPopUp />
      <FixedButtonArea />
    </div>
  );
}

export async function getStaticProps() {
  let data = {
    banners: [],
    products: [],
  };

  const response = await networkService.get(urlService.getSetup);

  if (response.IsValid) {
    data = { ...response.Payload };
  }

  // Get external data from the file system, API, DB, etc.
  const homeResponse = await networkService.get(urlService.getHome);

  if (homeResponse.IsValid) {
    const { banners, products } = homeResponse.Payload;

    data.banners = banners;
    data.products = products;
  }
  return {
    props: data,
  };
}
