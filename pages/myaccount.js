import SubHeader from "../components/common/subHeader";
import WhiteWrapper from "../components/partials/myAccount/whiteWrapper";
import useTranslation from "../services/useTranslation";

export default function MyAccount(){
    const {my_account} = useTranslation();
    return(
        <div>
 
  <SubHeader title= {my_account}/>
  <WhiteWrapper/>
</div>
    );
}