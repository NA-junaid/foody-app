import React from "react";
import SubHeader from "../components/common/subHeader";
import WhiteWrapper from "../components/partials/changePassword/whiteWrapper";
import useTranslation from "../services/useTranslation";
export default function ChangePassword() {
    const { change_password } = useTranslation();
    return (
        <div>
            <SubHeader title={change_password} />
            <WhiteWrapper />
        </div>
    );
}