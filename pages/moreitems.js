import WhiteWrapper from "../components/partials/moreItems/whiteWrapper";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import FixedButtonArea from "../components/fixedButtonArea";
import PopUp from "../components/partials/home/popUp";

export default function MoreItems() {
  const router = useRouter();
  let { category_id } = router.query;
  const products = useSelector((state) => state.products);
  let p = products.filter((i) => i.category_id == category_id);

  return (
    <div>
      <WhiteWrapper products={p} category_id={category_id} />
       <PopUp />
      <FixedButtonArea />
    </div>
  );
}
