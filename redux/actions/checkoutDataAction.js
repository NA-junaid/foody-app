export const updateCheckoutData = (payload) => {
  return {
    type: "UPDATE_CHECKOUT_DATA",
    payload,
  };
};

export const resetCheckoutData = () => {
  return {
    type: "RESET_CHECKOUT_DATA",
  };
};
