export default function routeReducer(state = false, action) {
    switch (action.type) {
        case 'SET_BACK':
            return action.payload;
        default:
            return state;
    }
}