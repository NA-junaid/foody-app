const initState = {
  name: "",
  email: "",
  phone: "",
  pickup: false,
  pickupOrDeliverySelected:false,
  location:'',
  outletId: null,
  area: null,
  areaInfo : {},
  paymentMethod: null,
  shippingMethod: null,
  shippingRate: 0,
  deliveryTime:'',
  isScheduled : false,
  direction:'',
  latitude : null,
  longitude: null,
  dis_type: '', 
  cop_discount: '', 
  discount_voucher_code: '', 
  cop_discount_amount:'',
  loyalty_reward_amount:0,

};

const checkoutDataReducer = (state = initState, action) => {

  switch (action.type) {
    /* Add item in cart item list with quantity 1 */
    case "UPDATE_CHECKOUT_DATA": {
      return {...action.payload};
    }
    case "RESET_CHECKOUT_DATA": {
      return initState;
    }
    default:
      return state;
  }
};

export default checkoutDataReducer;
