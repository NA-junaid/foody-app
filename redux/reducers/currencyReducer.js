const initialState = [];
  
  const currencyItem = (state, action) => {
    switch (action.type) {
        case 'SET_CURRENCY':
            return {
                ...state,
                isDefault: !state.isDefault
            }
        default:
            return state;
    }
  }
  
  export default function currencyReducer(state = initialState, action) {
    switch (action.type) {
      case "ADD_CURRENCY":
        let item = state.find(
          (item) => item.name == action.payload.name
        );
        if (item == undefined) {
          return [...state, action.payload];
        } else {
          return state;
        }
      case "SET_CURRENCY":
        return state.map(t => currencyItem(t, action));
      default:
        return state;
    }
  }
  