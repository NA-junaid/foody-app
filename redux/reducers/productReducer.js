const productItem = (state, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return action.payload;
        default:
            return state;
    }
}

export default function productReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_PRODUCT':
            let item = state.find(item =>  item.variant_id == action.payload.variant_id);
            if (item == undefined) {
                return [
                    ...state,
                    productItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}