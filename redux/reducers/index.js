import { combineReducers } from "redux";

import languageReducer from "./languageReducer";
import pageReducer from "./pageReducer";
import categoryReducer from "./categoryReducer";
import storeReducer from "./storeReducer";
import translationReducer from "./translationReducer";
import paymentMethodReducer from "./paymentMethodReducer";
import shippingMethodReducer from "./shippingMethodReducer";
import webSettingReducer from "./webSettingReducer";
import socialLinksReducer from "./socialLinksReducer";
import allCurrenciesReducer from "./allCurrenciesReducer";
import currencyReducer from "./currencyReducer";
import outletReducer from "./outletReducer";
import pluginReducer from "./pluginReducer";
import areaReducer from "./areaReducer";
import zoneReducer from "./zoneReducer";
import popupReducer from "./popupReducer";
import cartReducer from "./cartReducer";
import pickUpReducer from "./pickUpReducer";
import checkoutStepReducer from "./checkoutStepReducer";
import addonReducer from "./addonReducer";
import checkoutDataReducer from "./checkoutDataReducer";
import productReducer from "./productReducer";
import routeReducer from "./routeReducer";

export default combineReducers({
  languages: languageReducer,
  pages: pageReducer,
  categories: categoryReducer,
  store: storeReducer,
  translations: translationReducer,
  paymentMethods: paymentMethodReducer,
  shippingMethods: shippingMethodReducer,
  webSetting: webSettingReducer,
  socialLinks: socialLinksReducer,
  all_currencies: allCurrenciesReducer,
  currencies: currencyReducer,
  outlets: outletReducer,
  plugins: pluginReducer,
  areas: areaReducer,
  zones: zoneReducer,
  popup: popupReducer,
  cart: cartReducer,
  pickUp: pickUpReducer,
  checkoutStep: checkoutStepReducer,
  addons: addonReducer,
  checkoutData: checkoutDataReducer,
  products: productReducer,
  back: routeReducer,
});
