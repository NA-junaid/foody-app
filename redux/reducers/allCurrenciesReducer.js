const initialState = {  };

export default function allCurrenciesReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_ALLCURRIENCIES":
      return action.payload;
    default:
      return state;
  }
}
